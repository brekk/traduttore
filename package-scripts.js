module.exports = {
  scripts: {
    docs: "documentation build src/** -f html -o docs",
    readme: "documentation readme src/** --section=API --access=public",
    build: "rollup -c rollup.config.js",
    test: "jest --verbose",
    testWatch: "jest --watchAll --verbose --coverage",
    lint: "eslint src/*.js --env jest --fix",
    care: "nps docs build test lint readme"
  }
}
