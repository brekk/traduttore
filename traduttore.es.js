import { curry, pipe, toPairs, forEach, map, toUpper, split, addIndex, reduce } from 'ramda';
import { rail, fold } from 'handrail';

var I = function I(x) {
  return x;
};

var K = function K(x) {
  return function () {
    return x;
  };
};

var ERRORS = Object.freeze({
  NOT_A_NUMBER: "Expected input to be a number",
  NOT_A_STRING: "Expected input to be a string"
});

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function (obj) {
      return typeof obj;
    };
  } else {
    _typeof = function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

function _iterableToArrayLimit(arr, i) {
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance");
}

var isType = curry(function (x, y) {
  return _typeof(y) === x;
});
var isNumber = isType("number");
var isString = isType("string");

var handleErrors = function handleErrors(e) {
  throw new Error(e);
};

var sort = curry(function (fn, x) {
  return x.sort(fn);
});

var sortLookup = pipe(toPairs, sort(function (_ref, _ref2) {
  var _ref3 = _slicedToArray(_ref, 2),
      v = _ref3[1];
  var _ref4 = _slicedToArray(_ref2, 2),
      w = _ref4[1];
  return w - v;
}));

var encode = curry(function (system, input) {
  var lookup = sortLookup(system);
  var output = "";
  forEach(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        key = _ref2[0],
        value = _ref2[1];
    while (input >= value) {
      output += key;
      input -= value;
    }
  }, lookup);
  return output;
});

var safeNumeralSystem = curry(function (system, input) {
  return pipe(rail(isNumber, K(ERRORS.NOT_A_NUMBER)), map(encode(system)), fold(handleErrors, I))(input);
});

var ROMAN_NUMERAL_LOOKUP = Object.freeze({
  I: 1,
  IV: 4,
  V: 5,
  IX: 9,
  X: 10,
  XL: 40,
  L: 50,
  XC: 90,
  C: 100,
  CD: 400,
  D: 500,
  CM: 900,
  M: 1000
});

var romanEncode = safeNumeralSystem(ROMAN_NUMERAL_LOOKUP);

var reducer = function reducer(system) {
  return function (sum, _ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        current = _ref2[0],
        next = _ref2[1];
    var value = system[current];
    return sum + value * (value < system[next] ? -1 : 1);
  };
};

var decode = curry(function (system, input) {
  return pipe(toUpper, split(""), addIndex(map)(function (x, i, list) {
    return [x, list[i + 1]];
  }), reduce(reducer(system), 0))(input);
});

var safeDecode = curry(function (system, input) {
  return pipe(rail(isString, K(ERRORS.NOT_A_STRING)), map(decode(system)), fold(handleErrors, I))(input);
});

var romanDecode = safeDecode(ROMAN_NUMERAL_LOOKUP);

var index = {
  encode: safeNumeralSystem,
  decode: safeDecode,
  romanEncode: romanEncode,
  romanDecode: romanDecode
};

export default index;
