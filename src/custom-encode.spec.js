/* global test */
import {
  random
  // iterate
} from "f-utility"
import encode from "./safe-encode"
import decode from "./safe-decode"
import t from "./utils/testing"

const CUSTOM_LOOKUP = {
  $: 1,
  "$@": 4,
  "@": 5,
  "$*": 9,
  "*": 10,
  "*!": 40,
  "!": 50,
  "*^": 90,
  "^": 100,
  "^?": 400,
  "?": 500,
  "^&": 900,
  "&": 1000
}

const convert = encode(CUSTOM_LOOKUP)
const deconvert = decode(CUSTOM_LOOKUP)

test("convert", () => {
  const input = random.floorMin(1, 4e3)
  t.is(deconvert(convert(input)), input)
})
test("randomly generated values", () => {
  t.is(deconvert("&^^^**$"), 1321)
  t.is(convert(1321), "&^^^**$")

  t.is(deconvert("&?^^*^$$$"), 1793)
  t.is(convert(1793), "&?^^*^$$$")

  t.is(deconvert("&&^&@$$$"), 2908)
  t.is(convert(2908), "&&^&@$$$")

  t.is(deconvert("&&&^&!@"), 3955)
  t.is(convert(3955), "&&&^&!@")

  t.is(deconvert("^!***$"), 181)
  t.is(convert(181), "^!***$")

  t.is(deconvert("&!**$*"), 1079)
  t.is(convert(1079), "&!**$*")

  t.is(deconvert("&&&^?*^$@"), 3494)
  t.is(convert(3494), "&&&^?*^$@")

  t.is(deconvert("&&?^!**$@"), 2674)
  t.is(convert(2674), "&&?^!**$@")

  t.is(deconvert("^?!*@$$$"), 468)
  t.is(convert(468), "^?!*@$$$")

  t.is(deconvert("^&*$$$"), 913)
  t.is(convert(913), "^&*$$$")

  t.is(deconvert("^^^*!$$"), 342)
  t.is(convert(342), "^^^*!$$")

  t.is(deconvert("^^^!***$"), 381)
  t.is(convert(381), "^^^!***$")

  t.is(deconvert("&^?$@"), 1404)
  t.is(convert(1404), "&^?$@")

  t.is(deconvert("&^&**$$"), 1922)
  t.is(convert(1922), "&^&**$$")

  t.is(deconvert("&&^^^!*$$$"), 2363)
  t.is(convert(2363), "&&^^^!*$$$")

  t.is(deconvert("&&&^@$$"), 3107)
  t.is(convert(3107), "&&&^@$$")

  t.is(deconvert("&&?^^^***$$$"), 2833)
  t.is(convert(2833), "&&?^^^***$$$")

  t.is(deconvert("&&&^?*$$"), 3412)
  t.is(convert(3412), "&&&^?*$$")

  t.is(deconvert("?**"), 520)
  t.is(convert(520), "?**")

  t.is(deconvert("&&&?^^*!$$$"), 3743)
  t.is(convert(3743), "&&&?^^*!$$$")

  t.is(deconvert("&&?^^!***$"), 2781)
  t.is(convert(2781), "&&?^^!***$")

  t.is(deconvert("&&&^*$*"), 3119)
  t.is(convert(3119), "&&&^*$*")

  t.is(deconvert("&&&***$"), 3031)
  t.is(convert(3031), "&&&***$")

  t.is(deconvert("&&?^^!*$"), 2761)
  t.is(convert(2761), "&&?^^!*$")

  t.is(deconvert("&^$$$"), 1103)
  t.is(convert(1103), "&^$$$")

  t.is(deconvert("&&&?^^*^@$$"), 3797)
  t.is(convert(3797), "&&&?^^*^@$$")

  t.is(deconvert("&&&?^^^!*@$"), 3866)
  t.is(convert(3866), "&&&?^^^!*@$")

  t.is(deconvert("&&&^?!$"), 3451)
  t.is(convert(3451), "&&&^?!$")

  t.is(deconvert("^!**"), 170)
  t.is(convert(170), "^!**")

  t.is(deconvert("&^&$$$"), 1903)
  t.is(convert(1903), "&^&$$$")
})

// iterate(30, () => {
//   const input = random.floorMin(0, 4e3)
//   const output = convert(input)
//
//     `t.is(deconvert("${output}"), ${input})\nt.is(convert(${input}), "${output}")`
//   )
// })
