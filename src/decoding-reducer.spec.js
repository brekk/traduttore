/* global test */
import reducer from "./decoding-reducer"
import t from "./utils/testing"
import lookup from "./constants/roman-numeral-lookup"

test("reducer", () => {
  t.is(reducer(lookup)(0, ["I", "I"]), 1)
  t.is(reducer(lookup)(0, ["I", "IV"]), -1)
})
