import { map, curry, pipe } from "ramda"
import { rail, fold } from "handrail"
import I from "./utils/identity"
import K from "./utils/constant"
import ERRORS from "./constants/errors"
import { isNumber } from "./utils/types"
import handleError from "./utils/handle-errors"
import encode from "./encode"

/**
 * This method wraps `encode` with some potential error handling
 * Right now the error handling just involves `throw`ing the given string as an error
 * But we could make it more robust in the future
 * @method encode
 * @public
 * @param {object} system - a lookup map of {numeral: value}s
 * @param {number} input - a number to convert
 * @returns {string} an encoded value
 */
const safeNumeralSystem = curry((system, input) =>
  pipe(
    rail(isNumber, K(ERRORS.NOT_A_NUMBER)),
    map(encode(system)),
    fold(handleError, I)
  )(input)
)
export default safeNumeralSystem
