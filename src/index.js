import romanEncode from "./roman-encode"
import encode from "./safe-encode"
import romanDecode from "./roman-decode"
import decode from "./safe-decode"

export default {
  encode,
  decode,
  romanEncode,
  romanDecode
}
