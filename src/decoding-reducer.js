/**
 * This is the internal logic for the decode method
 * @private
 * @method reducer
 * @param {object} system - a lookup object
 * @param {number} sum - a number
 * @param {[string, string]} tuple - a tuple of keys that need to be in the given system lookup
 * @returns {number} a number
 */
const reducer = system => (sum, [current, next]) => {
  const value = system[current]
  return sum + value * (value < system[next] ? -1 : 1)
}
export default reducer
