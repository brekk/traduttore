import { curry, forEach } from "ramda"
import sortLookup from "./utils/sort-lookup"

/**
 * A function to encode a number given a mapping object
 * @method encode
 * @private
 * @param {object} system - a lookup map of {numeral: value}s
 * @param {number} input - a number to convert
 * @returns {string} an encoded value
 */
const encode = curry((system, input) => {
  const lookup = sortLookup(system)
  let output = ""
  forEach(([key, value]) => {
    while (input >= value) {
      output += key
      input -= value
    }
  }, lookup)
  return output
})

export default encode
