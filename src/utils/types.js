import { curry } from "ramda"

/**
 * A simple curried function for typeof assertions
 * @method isType
 * @param {string} x - a value to use in typeof comparison
 * @param {any} y - anything
 * @returns {boolean} - a boolean representing whether y is typeof x
 */
/* eslint-disable valid-typeof */
export const isType = curry((x, y) => typeof y === x)
export const isNumber = isType("number")
export const isString = isType("string")
