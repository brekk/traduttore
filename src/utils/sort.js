import { curry } from "ramda"

/**
 * This is a generic sort function with inverted parameters for easy piping
 * @method sort
 * @param {function} fn - a sorting function
 * @param {any[]} x - an array of stuff
 */
const sort = curry((fn, x) => x.sort(fn))
export default sort
