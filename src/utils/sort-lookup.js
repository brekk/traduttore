import { toPairs, pipe } from "ramda"
import sort from "./sort"

/**
 * This method allows us to use a single constant object as a
 * lookup for both encoding and decoding
 * @method sortLookup
 * @param {object} lookup - a {numeral: value} object
 * @returns {array[string, number]} - a sorted list of [numeral, value] tuples
 */
const sortLookup = pipe(
  toPairs,
  sort(([, v], [, w]) => w - v)
)
export default sortLookup
