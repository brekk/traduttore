/**
 * This method simply throws whatever it is given as an error
 * Right now this is not ideal but since we don't have much context
 * for the potential use cases here, this seems fine for now, and provides
 * an escape hatch for future error handling
 * @method handleErrors
 * @param {string} e - ostensibly, a string representing an error
 * @returns {null} - this method doesn't return anything
 */
const handleErrors = e => {
  throw new Error(e)
}
export default handleErrors
