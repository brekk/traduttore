/* global expect */

// for the purposes of testing,
// I find it much easier to have unary / binary functions
// rather than chained assertions
const t = {
  // truthy: x => expect(x).toBeTruthy(),
  // falsy: x => expect(x).toBeFalsy(),
  is: (x, y) => expect(x).toEqual(y),
  throws: (f, e) => expect(f).toThrow(e)
}
export default t
