/**
 * This is the constant combinator, a simple wrapper for () =>
 * @method K
 * @alias constant
 * @param {any} x - anything
 * @returns {function} which returns x
 */
const K = x => () => x
export default K
