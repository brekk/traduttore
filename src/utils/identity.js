/**
 * This is the identity combinator
 * It will return whatever it is given
 * In this particular case we're using it to pull values out of the Either monad
 * @method I
 * @alias identity
 * @param {any} x - anything
 * @returns {any} x
 */
const I = x => x
export default I
