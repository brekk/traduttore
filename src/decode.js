import { addIndex, map, reduce, curry, toUpper, split, pipe } from "ramda"
import reducer from "./decoding-reducer"

/**
 * A decode function which takes a lookup object and a string value and returns a number
 * @public
 * @method decode
 * @param {object} system - a lookup object of {numeral: value}s
 * @param {string} input - a numeral to look up by value
 * @returns {number} a decoded string
 */
const decode = curry((system, input) =>
  pipe(
    toUpper,
    split(""),
    addIndex(map)((x, i, list) => [x, list[i + 1]]),
    reduce(reducer(system), 0)
  )(input)
)
export default decode
