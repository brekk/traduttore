import { map, curry, pipe } from "ramda"
import { rail, fold } from "handrail"
import I from "./utils/identity"
import K from "./utils/constant"
import ERRORS from "./constants/errors"
import { isString } from "./utils/types"
import handleError from "./utils/handle-errors"
import decode from "./decode"

/**
 * This wraps decode with some potential error case handling
 * Right now the error case handling is just to `throw` but we have
 * the ability to handle more errors in the future
 * @private
 * @method safeDecode
 * @param {object} system - a lookup map
 * @param {string} input - a string to decode
 * @returns {number} - decoded number
 */
const safeDecode = curry((system, input) =>
  pipe(
    rail(isString, K(ERRORS.NOT_A_STRING)),
    map(decode(system)),
    fold(handleError, I)
  )(input)
)
export default safeDecode
