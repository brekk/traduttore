/**
 * A frozen object of error strings
 * @constant ERRORS
 * @private
 */
const ERRORS = Object.freeze({
  NOT_A_NUMBER: "Expected input to be a number",
  NOT_A_STRING: "Expected input to be a string"
})
export default ERRORS
