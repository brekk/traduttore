/**
 * A frozen object of roman numeral to arabic number mappings
 * @constant ROMAN_NUMERAL_LOOKUP
 * @private
 */
const ROMAN_NUMERAL_LOOKUP = Object.freeze({
  I: 1,
  IV: 4,
  V: 5,
  IX: 9,
  X: 10,
  XL: 40,
  L: 50,
  XC: 90,
  C: 100,
  CD: 400,
  D: 500,
  CM: 900,
  M: 1000
})
export default ROMAN_NUMERAL_LOOKUP
