import ROMAN_NUMERAL_LOOKUP from "./constants/roman-numeral-lookup"
import safeDecode from "./safe-decode"

/**
 * This is the core roman numeral to arabic number deconversion method
 * @public
 * @method romanDecode
 * @param {string} input - a string to decode
 * @returns {number} - decoded number
 */
export default safeDecode(ROMAN_NUMERAL_LOOKUP)
