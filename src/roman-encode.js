import encode from "./safe-encode"
import ROMAN_NUMERAL_LOOKUP from "./constants/roman-numeral-lookup"

/**
 * This is the core arabic number to roman conversion method
 * @method romanEncode
 * @public
 * @param {number} input - a number to convert
 * @returns {string} an encoded value
 */
const romanEncode = encode(ROMAN_NUMERAL_LOOKUP)
export default romanEncode
