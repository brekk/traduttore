/* global test */
import {
  // iterate,
  random
} from "f-utility"
import roman from "./roman-encode"
import arabic from "./roman-decode"
import ERRORS from "./constants/errors"
import t from "./utils/testing"

test("roman error cases", () => {
  t.throws(() => roman("not-a-number"), ERRORS.NOT_A_NUMBER)
  t.throws(() => roman(false), ERRORS.NOT_A_NUMBER)
})

test("roman success cases", () => {
  t.is(roman(1), "I")
  t.is(roman(2), "II")
  t.is(roman(3), "III")
  t.is(roman(5), "V")
  t.is(roman(6), "VI")
  t.is(roman(7), "VII")
  t.is(roman(8), "VIII")
  t.is(roman(10), "X")
  t.is(roman(15), "XV")
  t.is(roman(16), "XVI")
})

test("roman subtractive notation", () => {
  t.is(roman(4), "IV")
  t.is(roman(9), "IX")
  t.is(roman(40), "XL")
  t.is(roman(90), "XC")
  t.is(roman(400), "CD")
  t.is(roman(900), "CM")
})

test("arabic error cases", () => {
  t.throws(() => arabic(203933), ERRORS.NOT_A_STRING)
})

test("roman => arabic", () => {
  t.is(arabic("I"), 1)
  t.is(arabic("II"), 2)
  t.is(arabic("III"), 3)
  t.is(arabic("V"), 5)
  t.is(arabic("VI"), 6)
  t.is(arabic("VII"), 7)
  t.is(arabic("VIII"), 8)
  t.is(arabic("X"), 10)
  t.is(arabic("XV"), 15)
  t.is(arabic("XVI"), 16)
})

// generate some test cases
/*
iterate(30, () => {
  const input = random.floorMin(0, 4e3)
  const output = roman(input)
  console.log(
    `t.is(arabic("${output}"), ${input})\nt.is(roman(${input}), "${output}")`
  )
})
*/
test("roman + arabic testing", () => {
  t.is(arabic("DXXI"), 521)
  t.is(roman(521), "DXXI")

  t.is(arabic("MMMXXI"), 3021)
  t.is(roman(3021), "MMMXXI")

  t.is(arabic("DXLIX"), 549)
  t.is(roman(549), "DXLIX")

  t.is(arabic("MDLXXV"), 1575)
  t.is(roman(1575), "MDLXXV")

  t.is(arabic("MMDCCCLXXI"), 2871)
  t.is(roman(2871), "MMDCCCLXXI")

  t.is(arabic("MMCCCLXIII"), 2363)
  t.is(roman(2363), "MMCCCLXIII")

  t.is(arabic("MMMDCCCIX"), 3809)
  t.is(roman(3809), "MMMDCCCIX")

  t.is(arabic("MMCCCLXIX"), 2369)
  t.is(roman(2369), "MMCCCLXIX")

  t.is(arabic("MMMDCXL"), 3640)
  t.is(roman(3640), "MMMDCXL")

  t.is(arabic("DCCCLXXVII"), 877)
  t.is(roman(877), "DCCCLXXVII")

  t.is(arabic("MCMXXVII"), 1927)
  t.is(roman(1927), "MCMXXVII")

  t.is(arabic("MMCDXCIII"), 2493)
  t.is(roman(2493), "MMCDXCIII")

  t.is(arabic("MMMCXLIII"), 3143)
  t.is(roman(3143), "MMMCXLIII")

  t.is(arabic("CCXLIX"), 249)
  t.is(roman(249), "CCXLIX")

  t.is(arabic("MMDCLXXII"), 2672)
  t.is(roman(2672), "MMDCLXXII")

  t.is(arabic("MMMDCCLVIII"), 3758)
  t.is(roman(3758), "MMMDCCLVIII")

  t.is(arabic("MMCVI"), 2106)
  t.is(roman(2106), "MMCVI")

  t.is(arabic("MCCCLV"), 1355)
  t.is(roman(1355), "MCCCLV")

  t.is(arabic("MDCXXVIII"), 1628)
  t.is(roman(1628), "MDCXXVIII")

  t.is(arabic("MMMCMXLIV"), 3944)
  t.is(roman(3944), "MMMCMXLIV")

  t.is(arabic("MCCCXXI"), 1321)
  t.is(roman(1321), "MCCCXXI")

  t.is(arabic("MDCCXVII"), 1717)
  t.is(roman(1717), "MDCCXVII")

  t.is(arabic("MCMLXI"), 1961)
  t.is(roman(1961), "MCMLXI")

  t.is(arabic("MMMDCCXVII"), 3717)
  t.is(roman(3717), "MMMDCCXVII")

  t.is(arabic("MCCV"), 1205)
  t.is(roman(1205), "MCCV")

  t.is(arabic("MDXCV"), 1595)
  t.is(roman(1595), "MDXCV")

  t.is(arabic("MMMCL"), 3150)
  t.is(roman(3150), "MMMCL")

  t.is(arabic("MMCCCXXX"), 2330)
  t.is(roman(2330), "MMCCCXXX")
})
test("random", () => {
  const input = random.floorMin(0, 4e3)
  t.is(arabic(roman(input)), input)
})
