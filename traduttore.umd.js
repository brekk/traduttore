(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = global || self, global.traduttore = factory());
}(this, function () { 'use strict';

  function _typeof(obj) {
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
      _typeof = function (obj) {
        return typeof obj;
      };
    } else {
      _typeof = function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
      };
    }

    return _typeof(obj);
  }

  function _slicedToArray(arr, i) {
    return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest();
  }

  function _arrayWithHoles(arr) {
    if (Array.isArray(arr)) return arr;
  }

  function _iterableToArrayLimit(arr, i) {
    var _arr = [];
    var _n = true;
    var _d = false;
    var _e = undefined;

    try {
      for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
        _arr.push(_s.value);

        if (i && _arr.length === i) break;
      }
    } catch (err) {
      _d = true;
      _e = err;
    } finally {
      try {
        if (!_n && _i["return"] != null) _i["return"]();
      } finally {
        if (_d) throw _e;
      }
    }

    return _arr;
  }

  function _nonIterableRest() {
    throw new TypeError("Invalid attempt to destructure non-iterable instance");
  }

  function _isPlaceholder(a) {
    return a != null && _typeof(a) === 'object' && a['@@functional/placeholder'] === true;
  }

  function _curry1(fn) {
    return function f1(a) {
      if (arguments.length === 0 || _isPlaceholder(a)) {
        return f1;
      } else {
        return fn.apply(this, arguments);
      }
    };
  }

  function _curry2(fn) {
    return function f2(a, b) {
      switch (arguments.length) {
        case 0:
          return f2;
        case 1:
          return _isPlaceholder(a) ? f2 : _curry1(function (_b) {
            return fn(a, _b);
          });
        default:
          return _isPlaceholder(a) && _isPlaceholder(b) ? f2 : _isPlaceholder(a) ? _curry1(function (_a) {
            return fn(_a, b);
          }) : _isPlaceholder(b) ? _curry1(function (_b) {
            return fn(a, _b);
          }) : fn(a, b);
      }
    };
  }

  var add =
  _curry2(function add(a, b) {
    return Number(a) + Number(b);
  });

  function _concat(set1, set2) {
    set1 = set1 || [];
    set2 = set2 || [];
    var idx;
    var len1 = set1.length;
    var len2 = set2.length;
    var result = [];
    idx = 0;
    while (idx < len1) {
      result[result.length] = set1[idx];
      idx += 1;
    }
    idx = 0;
    while (idx < len2) {
      result[result.length] = set2[idx];
      idx += 1;
    }
    return result;
  }

  function _arity(n, fn) {
    switch (n) {
      case 0:
        return function () {
          return fn.apply(this, arguments);
        };
      case 1:
        return function (a0) {
          return fn.apply(this, arguments);
        };
      case 2:
        return function (a0, a1) {
          return fn.apply(this, arguments);
        };
      case 3:
        return function (a0, a1, a2) {
          return fn.apply(this, arguments);
        };
      case 4:
        return function (a0, a1, a2, a3) {
          return fn.apply(this, arguments);
        };
      case 5:
        return function (a0, a1, a2, a3, a4) {
          return fn.apply(this, arguments);
        };
      case 6:
        return function (a0, a1, a2, a3, a4, a5) {
          return fn.apply(this, arguments);
        };
      case 7:
        return function (a0, a1, a2, a3, a4, a5, a6) {
          return fn.apply(this, arguments);
        };
      case 8:
        return function (a0, a1, a2, a3, a4, a5, a6, a7) {
          return fn.apply(this, arguments);
        };
      case 9:
        return function (a0, a1, a2, a3, a4, a5, a6, a7, a8) {
          return fn.apply(this, arguments);
        };
      case 10:
        return function (a0, a1, a2, a3, a4, a5, a6, a7, a8, a9) {
          return fn.apply(this, arguments);
        };
      default:
        throw new Error('First argument to _arity must be a non-negative integer no greater than ten');
    }
  }

  function _curryN(length, received, fn) {
    return function () {
      var combined = [];
      var argsIdx = 0;
      var left = length;
      var combinedIdx = 0;
      while (combinedIdx < received.length || argsIdx < arguments.length) {
        var result;
        if (combinedIdx < received.length && (!_isPlaceholder(received[combinedIdx]) || argsIdx >= arguments.length)) {
          result = received[combinedIdx];
        } else {
          result = arguments[argsIdx];
          argsIdx += 1;
        }
        combined[combinedIdx] = result;
        if (!_isPlaceholder(result)) {
          left -= 1;
        }
        combinedIdx += 1;
      }
      return left <= 0 ? fn.apply(this, combined) : _arity(left, _curryN(length, combined, fn));
    };
  }

  var curryN =
  _curry2(function curryN(length, fn) {
    if (length === 1) {
      return _curry1(fn);
    }
    return _arity(length, _curryN(length, [], fn));
  });

  var addIndex =
  _curry1(function addIndex(fn) {
    return curryN(fn.length, function () {
      var idx = 0;
      var origFn = arguments[0];
      var list = arguments[arguments.length - 1];
      var args = Array.prototype.slice.call(arguments, 0);
      args[0] = function () {
        var result = origFn.apply(this, _concat(arguments, [idx, list]));
        idx += 1;
        return result;
      };
      return fn.apply(this, args);
    });
  });

  function _curry3(fn) {
    return function f3(a, b, c) {
      switch (arguments.length) {
        case 0:
          return f3;
        case 1:
          return _isPlaceholder(a) ? f3 : _curry2(function (_b, _c) {
            return fn(a, _b, _c);
          });
        case 2:
          return _isPlaceholder(a) && _isPlaceholder(b) ? f3 : _isPlaceholder(a) ? _curry2(function (_a, _c) {
            return fn(_a, b, _c);
          }) : _isPlaceholder(b) ? _curry2(function (_b, _c) {
            return fn(a, _b, _c);
          }) : _curry1(function (_c) {
            return fn(a, b, _c);
          });
        default:
          return _isPlaceholder(a) && _isPlaceholder(b) && _isPlaceholder(c) ? f3 : _isPlaceholder(a) && _isPlaceholder(b) ? _curry2(function (_a, _b) {
            return fn(_a, _b, c);
          }) : _isPlaceholder(a) && _isPlaceholder(c) ? _curry2(function (_a, _c) {
            return fn(_a, b, _c);
          }) : _isPlaceholder(b) && _isPlaceholder(c) ? _curry2(function (_b, _c) {
            return fn(a, _b, _c);
          }) : _isPlaceholder(a) ? _curry1(function (_a) {
            return fn(_a, b, c);
          }) : _isPlaceholder(b) ? _curry1(function (_b) {
            return fn(a, _b, c);
          }) : _isPlaceholder(c) ? _curry1(function (_c) {
            return fn(a, b, _c);
          }) : fn(a, b, c);
      }
    };
  }

  var _isArray = Array.isArray || function _isArray(val) {
    return val != null && val.length >= 0 && Object.prototype.toString.call(val) === '[object Array]';
  };

  function _isTransformer(obj) {
    return obj != null && typeof obj['@@transducer/step'] === 'function';
  }

  function _dispatchable(methodNames, xf, fn) {
    return function () {
      if (arguments.length === 0) {
        return fn();
      }
      var args = Array.prototype.slice.call(arguments, 0);
      var obj = args.pop();
      if (!_isArray(obj)) {
        var idx = 0;
        while (idx < methodNames.length) {
          if (typeof obj[methodNames[idx]] === 'function') {
            return obj[methodNames[idx]].apply(obj, args);
          }
          idx += 1;
        }
        if (_isTransformer(obj)) {
          var transducer = xf.apply(null, args);
          return transducer(obj);
        }
      }
      return fn.apply(this, arguments);
    };
  }

  var _xfBase = {
    init: function init() {
      return this.xf['@@transducer/init']();
    },
    result: function result(_result) {
      return this.xf['@@transducer/result'](_result);
    }
  };

  var max =
  _curry2(function max(a, b) {
    return b > a ? b : a;
  });

  function _map(fn, functor) {
    var idx = 0;
    var len = functor.length;
    var result = Array(len);
    while (idx < len) {
      result[idx] = fn(functor[idx]);
      idx += 1;
    }
    return result;
  }

  function _isString(x) {
    return Object.prototype.toString.call(x) === '[object String]';
  }

  var _isArrayLike =
  _curry1(function isArrayLike(x) {
    if (_isArray(x)) {
      return true;
    }
    if (!x) {
      return false;
    }
    if (_typeof(x) !== 'object') {
      return false;
    }
    if (_isString(x)) {
      return false;
    }
    if (x.nodeType === 1) {
      return !!x.length;
    }
    if (x.length === 0) {
      return true;
    }
    if (x.length > 0) {
      return x.hasOwnProperty(0) && x.hasOwnProperty(x.length - 1);
    }
    return false;
  });

  var XWrap =
  function () {
    function XWrap(fn) {
      this.f = fn;
    }
    XWrap.prototype['@@transducer/init'] = function () {
      throw new Error('init not implemented on XWrap');
    };
    XWrap.prototype['@@transducer/result'] = function (acc) {
      return acc;
    };
    XWrap.prototype['@@transducer/step'] = function (acc, x) {
      return this.f(acc, x);
    };
    return XWrap;
  }();
  function _xwrap(fn) {
    return new XWrap(fn);
  }

  var bind =
  _curry2(function bind(fn, thisObj) {
    return _arity(fn.length, function () {
      return fn.apply(thisObj, arguments);
    });
  });

  function _arrayReduce(xf, acc, list) {
    var idx = 0;
    var len = list.length;
    while (idx < len) {
      acc = xf['@@transducer/step'](acc, list[idx]);
      if (acc && acc['@@transducer/reduced']) {
        acc = acc['@@transducer/value'];
        break;
      }
      idx += 1;
    }
    return xf['@@transducer/result'](acc);
  }
  function _iterableReduce(xf, acc, iter) {
    var step = iter.next();
    while (!step.done) {
      acc = xf['@@transducer/step'](acc, step.value);
      if (acc && acc['@@transducer/reduced']) {
        acc = acc['@@transducer/value'];
        break;
      }
      step = iter.next();
    }
    return xf['@@transducer/result'](acc);
  }
  function _methodReduce(xf, acc, obj, methodName) {
    return xf['@@transducer/result'](obj[methodName](bind(xf['@@transducer/step'], xf), acc));
  }
  var symIterator = typeof Symbol !== 'undefined' ? Symbol.iterator : '@@iterator';
  function _reduce(fn, acc, list) {
    if (typeof fn === 'function') {
      fn = _xwrap(fn);
    }
    if (_isArrayLike(list)) {
      return _arrayReduce(fn, acc, list);
    }
    if (typeof list['fantasy-land/reduce'] === 'function') {
      return _methodReduce(fn, acc, list, 'fantasy-land/reduce');
    }
    if (list[symIterator] != null) {
      return _iterableReduce(fn, acc, list[symIterator]());
    }
    if (typeof list.next === 'function') {
      return _iterableReduce(fn, acc, list);
    }
    if (typeof list.reduce === 'function') {
      return _methodReduce(fn, acc, list, 'reduce');
    }
    throw new TypeError('reduce: list must be array or iterable');
  }

  var XMap =
  function () {
    function XMap(f, xf) {
      this.xf = xf;
      this.f = f;
    }
    XMap.prototype['@@transducer/init'] = _xfBase.init;
    XMap.prototype['@@transducer/result'] = _xfBase.result;
    XMap.prototype['@@transducer/step'] = function (result, input) {
      return this.xf['@@transducer/step'](result, this.f(input));
    };
    return XMap;
  }();
  var _xmap =
  _curry2(function _xmap(f, xf) {
    return new XMap(f, xf);
  });

  function _has(prop, obj) {
    return Object.prototype.hasOwnProperty.call(obj, prop);
  }

  var toString = Object.prototype.toString;
  var _isArguments =
  function () {
    return toString.call(arguments) === '[object Arguments]' ? function _isArguments(x) {
      return toString.call(x) === '[object Arguments]';
    } : function _isArguments(x) {
      return _has('callee', x);
    };
  }();

  var hasEnumBug = !
  {
    toString: null
  }.propertyIsEnumerable('toString');
  var nonEnumerableProps = ['constructor', 'valueOf', 'isPrototypeOf', 'toString', 'propertyIsEnumerable', 'hasOwnProperty', 'toLocaleString'];
  var hasArgsEnumBug =
  function () {
    return arguments.propertyIsEnumerable('length');
  }();
  var contains = function contains(list, item) {
    var idx = 0;
    while (idx < list.length) {
      if (list[idx] === item) {
        return true;
      }
      idx += 1;
    }
    return false;
  };
  var keys = typeof Object.keys === 'function' && !hasArgsEnumBug ?
  _curry1(function keys(obj) {
    return Object(obj) !== obj ? [] : Object.keys(obj);
  }) :
  _curry1(function keys(obj) {
    if (Object(obj) !== obj) {
      return [];
    }
    var prop, nIdx;
    var ks = [];
    var checkArgsLength = hasArgsEnumBug && _isArguments(obj);
    for (prop in obj) {
      if (_has(prop, obj) && (!checkArgsLength || prop !== 'length')) {
        ks[ks.length] = prop;
      }
    }
    if (hasEnumBug) {
      nIdx = nonEnumerableProps.length - 1;
      while (nIdx >= 0) {
        prop = nonEnumerableProps[nIdx];
        if (_has(prop, obj) && !contains(ks, prop)) {
          ks[ks.length] = prop;
        }
        nIdx -= 1;
      }
    }
    return ks;
  });

  var map =
  _curry2(
  _dispatchable(['fantasy-land/map', 'map'], _xmap, function map(fn, functor) {
    switch (Object.prototype.toString.call(functor)) {
      case '[object Function]':
        return curryN(functor.length, function () {
          return fn.call(this, functor.apply(this, arguments));
        });
      case '[object Object]':
        return _reduce(function (acc, key) {
          acc[key] = fn(functor[key]);
          return acc;
        }, {}, keys(functor));
      default:
        return _map(fn, functor);
    }
  }));

  var path =
  _curry2(function path(paths, obj) {
    var val = obj;
    var idx = 0;
    while (idx < paths.length) {
      if (val == null) {
        return;
      }
      val = val[paths[idx]];
      idx += 1;
    }
    return val;
  });

  var prop =
  _curry2(function prop(p, obj) {
    return path([p], obj);
  });

  var pluck =
  _curry2(function pluck(p, list) {
    return map(prop(p), list);
  });

  var reduce =
  _curry3(_reduce);

  var ap =
  _curry2(function ap(applyF, applyX) {
    return typeof applyX['fantasy-land/ap'] === 'function' ? applyX['fantasy-land/ap'](applyF) : typeof applyF.ap === 'function' ? applyF.ap(applyX) : typeof applyF === 'function' ? function (x) {
      return applyF(x)(applyX(x));
    } : _reduce(function (acc, f) {
      return _concat(acc, map(f, applyX));
    }, [], applyF);
  });

  function _isFunction(x) {
    return Object.prototype.toString.call(x) === '[object Function]';
  }

  var liftN =
  _curry2(function liftN(arity, fn) {
    var lifted = curryN(arity, fn);
    return curryN(arity, function () {
      return _reduce(ap, map(lifted, arguments[0]), Array.prototype.slice.call(arguments, 1));
    });
  });

  var lift =
  _curry1(function lift(fn) {
    return liftN(fn.length, fn);
  });

  var curry =
  _curry1(function curry(fn) {
    return curryN(fn.length, fn);
  });

  var call =
  curry(function call(fn) {
    return fn.apply(this, Array.prototype.slice.call(arguments, 1));
  });

  function _makeFlat(recursive) {
    return function flatt(list) {
      var value, jlen, j;
      var result = [];
      var idx = 0;
      var ilen = list.length;
      while (idx < ilen) {
        if (_isArrayLike(list[idx])) {
          value = recursive ? flatt(list[idx]) : list[idx];
          j = 0;
          jlen = value.length;
          while (j < jlen) {
            result[result.length] = value[j];
            j += 1;
          }
        } else {
          result[result.length] = list[idx];
        }
        idx += 1;
      }
      return result;
    };
  }

  function _forceReduced(x) {
    return {
      '@@transducer/value': x,
      '@@transducer/reduced': true
    };
  }

  var preservingReduced = function preservingReduced(xf) {
    return {
      '@@transducer/init': _xfBase.init,
      '@@transducer/result': function transducerResult(result) {
        return xf['@@transducer/result'](result);
      },
      '@@transducer/step': function transducerStep(result, input) {
        var ret = xf['@@transducer/step'](result, input);
        return ret['@@transducer/reduced'] ? _forceReduced(ret) : ret;
      }
    };
  };
  var _flatCat = function _xcat(xf) {
    var rxf = preservingReduced(xf);
    return {
      '@@transducer/init': _xfBase.init,
      '@@transducer/result': function transducerResult(result) {
        return rxf['@@transducer/result'](result);
      },
      '@@transducer/step': function transducerStep(result, input) {
        return !_isArrayLike(input) ? _reduce(rxf, result, [input]) : _reduce(rxf, result, input);
      }
    };
  };

  var _xchain =
  _curry2(function _xchain(f, xf) {
    return map(f, _flatCat(xf));
  });

  var chain =
  _curry2(
  _dispatchable(['fantasy-land/chain', 'chain'], _xchain, function chain(fn, monad) {
    if (typeof monad === 'function') {
      return function (x) {
        return fn(monad(x))(x);
      };
    }
    return _makeFlat(false)(map(fn, monad));
  }));

  var type =
  _curry1(function type(val) {
    return val === null ? 'Null' : val === undefined ? 'Undefined' : Object.prototype.toString.call(val).slice(8, -1);
  });

  var not =
  _curry1(function not(a) {
    return !a;
  });

  var complement =
  lift(not);

  function _pipe(f, g) {
    return function () {
      return g.call(this, f.apply(this, arguments));
    };
  }

  function _checkForMethod(methodname, fn) {
    return function () {
      var length = arguments.length;
      if (length === 0) {
        return fn();
      }
      var obj = arguments[length - 1];
      return _isArray(obj) || typeof obj[methodname] !== 'function' ? fn.apply(this, arguments) : obj[methodname].apply(obj, Array.prototype.slice.call(arguments, 0, length - 1));
    };
  }

  var slice =
  _curry3(
  _checkForMethod('slice', function slice(fromIndex, toIndex, list) {
    return Array.prototype.slice.call(list, fromIndex, toIndex);
  }));

  var tail =
  _curry1(
  _checkForMethod('tail',
  slice(1, Infinity)));

  function pipe() {
    if (arguments.length === 0) {
      throw new Error('pipe requires at least one argument');
    }
    return _arity(arguments[0].length, reduce(_pipe, arguments[0], tail(arguments)));
  }

  var reverse =
  _curry1(function reverse(list) {
    return _isString(list) ? list.split('').reverse().join('') : Array.prototype.slice.call(list, 0).reverse();
  });

  function compose() {
    if (arguments.length === 0) {
      throw new Error('compose requires at least one argument');
    }
    return pipe.apply(this, reverse(arguments));
  }

  var nth =
  _curry2(function nth(offset, list) {
    var idx = offset < 0 ? list.length + offset : offset;
    return _isString(list) ? list.charAt(idx) : list[idx];
  });

  var head =
  nth(0);

  function _identity(x) {
    return x;
  }

  var identity =
  _curry1(_identity);

  function _arrayFromIterator(iter) {
    var list = [];
    var next;
    while (!(next = iter.next()).done) {
      list.push(next.value);
    }
    return list;
  }

  function _includesWith(pred, x, list) {
    var idx = 0;
    var len = list.length;
    while (idx < len) {
      if (pred(x, list[idx])) {
        return true;
      }
      idx += 1;
    }
    return false;
  }

  function _functionName(f) {
    var match = String(f).match(/^function (\w*)/);
    return match == null ? '' : match[1];
  }

  function _objectIs(a, b) {
    if (a === b) {
      return a !== 0 || 1 / a === 1 / b;
    } else {
      return a !== a && b !== b;
    }
  }
  var _objectIs$1 = typeof Object.is === 'function' ? Object.is : _objectIs;

  function _uniqContentEquals(aIterator, bIterator, stackA, stackB) {
    var a = _arrayFromIterator(aIterator);
    var b = _arrayFromIterator(bIterator);
    function eq(_a, _b) {
      return _equals(_a, _b, stackA.slice(), stackB.slice());
    }
    return !_includesWith(function (b, aItem) {
      return !_includesWith(eq, aItem, b);
    }, b, a);
  }
  function _equals(a, b, stackA, stackB) {
    if (_objectIs$1(a, b)) {
      return true;
    }
    var typeA = type(a);
    if (typeA !== type(b)) {
      return false;
    }
    if (a == null || b == null) {
      return false;
    }
    if (typeof a['fantasy-land/equals'] === 'function' || typeof b['fantasy-land/equals'] === 'function') {
      return typeof a['fantasy-land/equals'] === 'function' && a['fantasy-land/equals'](b) && typeof b['fantasy-land/equals'] === 'function' && b['fantasy-land/equals'](a);
    }
    if (typeof a.equals === 'function' || typeof b.equals === 'function') {
      return typeof a.equals === 'function' && a.equals(b) && typeof b.equals === 'function' && b.equals(a);
    }
    switch (typeA) {
      case 'Arguments':
      case 'Array':
      case 'Object':
        if (typeof a.constructor === 'function' && _functionName(a.constructor) === 'Promise') {
          return a === b;
        }
        break;
      case 'Boolean':
      case 'Number':
      case 'String':
        if (!(_typeof(a) === _typeof(b) && _objectIs$1(a.valueOf(), b.valueOf()))) {
          return false;
        }
        break;
      case 'Date':
        if (!_objectIs$1(a.valueOf(), b.valueOf())) {
          return false;
        }
        break;
      case 'Error':
        return a.name === b.name && a.message === b.message;
      case 'RegExp':
        if (!(a.source === b.source && a.global === b.global && a.ignoreCase === b.ignoreCase && a.multiline === b.multiline && a.sticky === b.sticky && a.unicode === b.unicode)) {
          return false;
        }
        break;
    }
    var idx = stackA.length - 1;
    while (idx >= 0) {
      if (stackA[idx] === a) {
        return stackB[idx] === b;
      }
      idx -= 1;
    }
    switch (typeA) {
      case 'Map':
        if (a.size !== b.size) {
          return false;
        }
        return _uniqContentEquals(a.entries(), b.entries(), stackA.concat([a]), stackB.concat([b]));
      case 'Set':
        if (a.size !== b.size) {
          return false;
        }
        return _uniqContentEquals(a.values(), b.values(), stackA.concat([a]), stackB.concat([b]));
      case 'Arguments':
      case 'Array':
      case 'Object':
      case 'Boolean':
      case 'Number':
      case 'String':
      case 'Date':
      case 'Error':
      case 'RegExp':
      case 'Int8Array':
      case 'Uint8Array':
      case 'Uint8ClampedArray':
      case 'Int16Array':
      case 'Uint16Array':
      case 'Int32Array':
      case 'Uint32Array':
      case 'Float32Array':
      case 'Float64Array':
      case 'ArrayBuffer':
        break;
      default:
        return false;
    }
    var keysA = keys(a);
    if (keysA.length !== keys(b).length) {
      return false;
    }
    var extendedStackA = stackA.concat([a]);
    var extendedStackB = stackB.concat([b]);
    idx = keysA.length - 1;
    while (idx >= 0) {
      var key = keysA[idx];
      if (!(_has(key, b) && _equals(b[key], a[key], extendedStackA, extendedStackB))) {
        return false;
      }
      idx -= 1;
    }
    return true;
  }

  var equals =
  _curry2(function equals(a, b) {
    return _equals(a, b, [], []);
  });

  function _indexOf(list, a, idx) {
    var inf, item;
    if (typeof list.indexOf === 'function') {
      switch (_typeof(a)) {
        case 'number':
          if (a === 0) {
            inf = 1 / a;
            while (idx < list.length) {
              item = list[idx];
              if (item === 0 && 1 / item === inf) {
                return idx;
              }
              idx += 1;
            }
            return -1;
          } else if (a !== a) {
            while (idx < list.length) {
              item = list[idx];
              if (typeof item === 'number' && item !== item) {
                return idx;
              }
              idx += 1;
            }
            return -1;
          }
          return list.indexOf(a, idx);
        case 'string':
        case 'boolean':
        case 'function':
        case 'undefined':
          return list.indexOf(a, idx);
        case 'object':
          if (a === null) {
            return list.indexOf(a, idx);
          }
      }
    }
    while (idx < list.length) {
      if (equals(list[idx], a)) {
        return idx;
      }
      idx += 1;
    }
    return -1;
  }

  function _includes(a, list) {
    return _indexOf(list, a, 0) >= 0;
  }

  function _quote(s) {
    var escaped = s.replace(/\\/g, '\\\\').replace(/[\b]/g, '\\b')
    .replace(/\f/g, '\\f').replace(/\n/g, '\\n').replace(/\r/g, '\\r').replace(/\t/g, '\\t').replace(/\v/g, '\\v').replace(/\0/g, '\\0');
    return '"' + escaped.replace(/"/g, '\\"') + '"';
  }

  var pad = function pad(n) {
    return (n < 10 ? '0' : '') + n;
  };
  var _toISOString = typeof Date.prototype.toISOString === 'function' ? function _toISOString(d) {
    return d.toISOString();
  } : function _toISOString(d) {
    return d.getUTCFullYear() + '-' + pad(d.getUTCMonth() + 1) + '-' + pad(d.getUTCDate()) + 'T' + pad(d.getUTCHours()) + ':' + pad(d.getUTCMinutes()) + ':' + pad(d.getUTCSeconds()) + '.' + (d.getUTCMilliseconds() / 1000).toFixed(3).slice(2, 5) + 'Z';
  };

  function _complement(f) {
    return function () {
      return !f.apply(this, arguments);
    };
  }

  function _filter(fn, list) {
    var idx = 0;
    var len = list.length;
    var result = [];
    while (idx < len) {
      if (fn(list[idx])) {
        result[result.length] = list[idx];
      }
      idx += 1;
    }
    return result;
  }

  function _isObject(x) {
    return Object.prototype.toString.call(x) === '[object Object]';
  }

  var XFilter =
  function () {
    function XFilter(f, xf) {
      this.xf = xf;
      this.f = f;
    }
    XFilter.prototype['@@transducer/init'] = _xfBase.init;
    XFilter.prototype['@@transducer/result'] = _xfBase.result;
    XFilter.prototype['@@transducer/step'] = function (result, input) {
      return this.f(input) ? this.xf['@@transducer/step'](result, input) : result;
    };
    return XFilter;
  }();
  var _xfilter =
  _curry2(function _xfilter(f, xf) {
    return new XFilter(f, xf);
  });

  var filter =
  _curry2(
  _dispatchable(['filter'], _xfilter, function (pred, filterable) {
    return _isObject(filterable) ? _reduce(function (acc, key) {
      if (pred(filterable[key])) {
        acc[key] = filterable[key];
      }
      return acc;
    }, {}, keys(filterable)) :
    _filter(pred, filterable);
  }));

  var reject =
  _curry2(function reject(pred, filterable) {
    return filter(_complement(pred), filterable);
  });

  function _toString(x, seen) {
    var recur = function recur(y) {
      var xs = seen.concat([x]);
      return _includes(y, xs) ? '<Circular>' : _toString(y, xs);
    };
    var mapPairs = function mapPairs(obj, keys$$1) {
      return _map(function (k) {
        return _quote(k) + ': ' + recur(obj[k]);
      }, keys$$1.slice().sort());
    };
    switch (Object.prototype.toString.call(x)) {
      case '[object Arguments]':
        return '(function() { return arguments; }(' + _map(recur, x).join(', ') + '))';
      case '[object Array]':
        return '[' + _map(recur, x).concat(mapPairs(x, reject(function (k) {
          return /^\d+$/.test(k);
        }, keys(x)))).join(', ') + ']';
      case '[object Boolean]':
        return _typeof(x) === 'object' ? 'new Boolean(' + recur(x.valueOf()) + ')' : x.toString();
      case '[object Date]':
        return 'new Date(' + (isNaN(x.valueOf()) ? recur(NaN) : _quote(_toISOString(x))) + ')';
      case '[object Null]':
        return 'null';
      case '[object Number]':
        return _typeof(x) === 'object' ? 'new Number(' + recur(x.valueOf()) + ')' : 1 / x === -Infinity ? '-0' : x.toString(10);
      case '[object String]':
        return _typeof(x) === 'object' ? 'new String(' + recur(x.valueOf()) + ')' : _quote(x);
      case '[object Undefined]':
        return 'undefined';
      default:
        if (typeof x.toString === 'function') {
          var repr = x.toString();
          if (repr !== '[object Object]') {
            return repr;
          }
        }
        return '{' + mapPairs(x, keys(x)).join(', ') + '}';
    }
  }

  var toString$1 =
  _curry1(function toString(val) {
    return _toString(val, []);
  });

  var converge =
  _curry2(function converge(after, fns) {
    return curryN(reduce(max, 0, pluck('length', fns)), function () {
      var args = arguments;
      var context = this;
      return after.apply(context, _map(function (fn) {
        return fn.apply(context, args);
      }, fns));
    });
  });

  var XReduceBy =
  function () {
    function XReduceBy(valueFn, valueAcc, keyFn, xf) {
      this.valueFn = valueFn;
      this.valueAcc = valueAcc;
      this.keyFn = keyFn;
      this.xf = xf;
      this.inputs = {};
    }
    XReduceBy.prototype['@@transducer/init'] = _xfBase.init;
    XReduceBy.prototype['@@transducer/result'] = function (result) {
      var key;
      for (key in this.inputs) {
        if (_has(key, this.inputs)) {
          result = this.xf['@@transducer/step'](result, this.inputs[key]);
          if (result['@@transducer/reduced']) {
            result = result['@@transducer/value'];
            break;
          }
        }
      }
      this.inputs = null;
      return this.xf['@@transducer/result'](result);
    };
    XReduceBy.prototype['@@transducer/step'] = function (result, input) {
      var key = this.keyFn(input);
      this.inputs[key] = this.inputs[key] || [key, this.valueAcc];
      this.inputs[key][1] = this.valueFn(this.inputs[key][1], input);
      return result;
    };
    return XReduceBy;
  }();
  var _xreduceBy =
  _curryN(4, [], function _xreduceBy(valueFn, valueAcc, keyFn, xf) {
    return new XReduceBy(valueFn, valueAcc, keyFn, xf);
  });

  var reduceBy =
  _curryN(4, [],
  _dispatchable([], _xreduceBy, function reduceBy(valueFn, valueAcc, keyFn, list) {
    return _reduce(function (acc, elt) {
      var key = keyFn(elt);
      acc[key] = valueFn(_has(key, acc) ? acc[key] : valueAcc, elt);
      return acc;
    }, {}, list);
  }));

  var countBy =
  reduceBy(function (acc, elem) {
    return acc + 1;
  }, 0);

  var dec =
  add(-1);

  var _Set =
  function () {
    function _Set() {
      this._nativeSet = typeof Set === 'function' ? new Set() : null;
      this._items = {};
    }
    _Set.prototype.add = function (item) {
      return !hasOrAdd(item, true, this);
    };
    _Set.prototype.has = function (item) {
      return hasOrAdd(item, false, this);
    };
    return _Set;
  }();
  function hasOrAdd(item, shouldAdd, set) {
    var type = _typeof(item);
    var prevSize, newSize;
    switch (type) {
      case 'string':
      case 'number':
        if (item === 0 && 1 / item === -Infinity) {
          if (set._items['-0']) {
            return true;
          } else {
            if (shouldAdd) {
              set._items['-0'] = true;
            }
            return false;
          }
        }
        if (set._nativeSet !== null) {
          if (shouldAdd) {
            prevSize = set._nativeSet.size;
            set._nativeSet.add(item);
            newSize = set._nativeSet.size;
            return newSize === prevSize;
          } else {
            return set._nativeSet.has(item);
          }
        } else {
          if (!(type in set._items)) {
            if (shouldAdd) {
              set._items[type] = {};
              set._items[type][item] = true;
            }
            return false;
          } else if (item in set._items[type]) {
            return true;
          } else {
            if (shouldAdd) {
              set._items[type][item] = true;
            }
            return false;
          }
        }
      case 'boolean':
        if (type in set._items) {
          var bIdx = item ? 1 : 0;
          if (set._items[type][bIdx]) {
            return true;
          } else {
            if (shouldAdd) {
              set._items[type][bIdx] = true;
            }
            return false;
          }
        } else {
          if (shouldAdd) {
            set._items[type] = item ? [false, true] : [true, false];
          }
          return false;
        }
      case 'function':
        if (set._nativeSet !== null) {
          if (shouldAdd) {
            prevSize = set._nativeSet.size;
            set._nativeSet.add(item);
            newSize = set._nativeSet.size;
            return newSize === prevSize;
          } else {
            return set._nativeSet.has(item);
          }
        } else {
          if (!(type in set._items)) {
            if (shouldAdd) {
              set._items[type] = [item];
            }
            return false;
          }
          if (!_includes(item, set._items[type])) {
            if (shouldAdd) {
              set._items[type].push(item);
            }
            return false;
          }
          return true;
        }
      case 'undefined':
        if (set._items[type]) {
          return true;
        } else {
          if (shouldAdd) {
            set._items[type] = true;
          }
          return false;
        }
      case 'object':
        if (item === null) {
          if (!set._items['null']) {
            if (shouldAdd) {
              set._items['null'] = true;
            }
            return false;
          }
          return true;
        }
      default:
        type = Object.prototype.toString.call(item);
        if (!(type in set._items)) {
          if (shouldAdd) {
            set._items[type] = [item];
          }
          return false;
        }
        if (!_includes(item, set._items[type])) {
          if (shouldAdd) {
            set._items[type].push(item);
          }
          return false;
        }
        return true;
    }
  }

  var XDropRepeatsWith =
  function () {
    function XDropRepeatsWith(pred, xf) {
      this.xf = xf;
      this.pred = pred;
      this.lastValue = undefined;
      this.seenFirstValue = false;
    }
    XDropRepeatsWith.prototype['@@transducer/init'] = _xfBase.init;
    XDropRepeatsWith.prototype['@@transducer/result'] = _xfBase.result;
    XDropRepeatsWith.prototype['@@transducer/step'] = function (result, input) {
      var sameAsLast = false;
      if (!this.seenFirstValue) {
        this.seenFirstValue = true;
      } else if (this.pred(this.lastValue, input)) {
        sameAsLast = true;
      }
      this.lastValue = input;
      return sameAsLast ? result : this.xf['@@transducer/step'](result, input);
    };
    return XDropRepeatsWith;
  }();
  var _xdropRepeatsWith =
  _curry2(function _xdropRepeatsWith(pred, xf) {
    return new XDropRepeatsWith(pred, xf);
  });

  var last =
  nth(-1);

  var dropRepeatsWith =
  _curry2(
  _dispatchable([], _xdropRepeatsWith, function dropRepeatsWith(pred, list) {
    var result = [];
    var idx = 1;
    var len = list.length;
    if (len !== 0) {
      result[0] = list[0];
      while (idx < len) {
        if (!pred(last(result), list[idx])) {
          result[result.length] = list[idx];
        }
        idx += 1;
      }
    }
    return result;
  }));

  var dropRepeats =
  _curry1(
  _dispatchable([],
  _xdropRepeatsWith(equals),
  dropRepeatsWith(equals)));

  var flip =
  _curry1(function flip(fn) {
    return curryN(fn.length, function (a, b) {
      var args = Array.prototype.slice.call(arguments, 0);
      args[0] = b;
      args[1] = a;
      return fn.apply(this, args);
    });
  });

  var forEach =
  _curry2(
  _checkForMethod('forEach', function forEach(fn, list) {
    var len = list.length;
    var idx = 0;
    while (idx < len) {
      fn(list[idx]);
      idx += 1;
    }
    return list;
  }));

  var groupBy =
  _curry2(
  _checkForMethod('groupBy',
  reduceBy(function (acc, item) {
    if (acc == null) {
      acc = [];
    }
    acc.push(item);
    return acc;
  }, null)));

  var inc =
  add(1);

  var indexBy =
  reduceBy(function (acc, elem) {
    return elem;
  }, null);

  var init =
  slice(0, -1);

  var uniqBy =
  _curry2(function uniqBy(fn, list) {
    var set = new _Set();
    var result = [];
    var idx = 0;
    var appliedItem, item;
    while (idx < list.length) {
      item = list[idx];
      appliedItem = fn(item);
      if (set.add(appliedItem)) {
        result.push(item);
      }
      idx += 1;
    }
    return result;
  });

  var uniq =
  uniqBy(identity);

  var invoker =
  _curry2(function invoker(arity, method) {
    return curryN(arity + 1, function () {
      var target = arguments[arity];
      if (target != null && _isFunction(target[method])) {
        return target[method].apply(target, Array.prototype.slice.call(arguments, 0, arity));
      }
      throw new TypeError(toString$1(target) + ' does not have a method named "' + method + '"');
    });
  });

  var join =
  invoker(1, 'join');

  var juxt =
  _curry1(function juxt(fns) {
    return converge(function () {
      return Array.prototype.slice.call(arguments, 0);
    }, fns);
  });

  var sum =
  reduce(add, 0);

  var multiply =
  _curry2(function multiply(a, b) {
    return a * b;
  });

  function _createPartialApplicator(concat) {
    return _curry2(function (fn, args) {
      return _arity(Math.max(0, fn.length - args.length), function () {
        return fn.apply(this, concat(args, arguments));
      });
    });
  }

  var partialRight =
  _createPartialApplicator(
  flip(_concat));

  var partition =
  juxt([filter, reject]);

  var pickAll =
  _curry2(function pickAll(names, obj) {
    var result = {};
    var idx = 0;
    var len = names.length;
    while (idx < len) {
      var name = names[idx];
      result[name] = obj[name];
      idx += 1;
    }
    return result;
  });

  var product =
  reduce(multiply, 1);

  var useWith =
  _curry2(function useWith(fn, transformers) {
    return curryN(transformers.length, function () {
      var args = [];
      var idx = 0;
      while (idx < transformers.length) {
        args.push(transformers[idx].call(this, arguments[idx]));
        idx += 1;
      }
      return fn.apply(this, args.concat(Array.prototype.slice.call(arguments, transformers.length)));
    });
  });

  var project =
  useWith(_map, [pickAll, identity]);

  var split =
  invoker(1, 'split');

  var toLower =
  invoker(0, 'toLowerCase');

  var toPairs =
  _curry1(function toPairs(obj) {
    var pairs = [];
    for (var prop in obj) {
      if (_has(prop, obj)) {
        pairs[pairs.length] = [prop, obj[prop]];
      }
    }
    return pairs;
  });

  var toUpper =
  invoker(0, 'toUpperCase');

  var transduce =
  curryN(4, function transduce(xf, fn, acc, list) {
    return _reduce(xf(typeof fn === 'function' ? _xwrap(fn) : fn), acc, list);
  });

  var ws = "\t\n\x0B\f\r \xA0\u1680\u180E\u2000\u2001\u2002\u2003" + "\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000\u2028" + "\u2029\uFEFF";
  var zeroWidth = "\u200B";
  var hasProtoTrim = typeof String.prototype.trim === 'function';
  var trim = !hasProtoTrim ||
  ws.trim() || !
  zeroWidth.trim() ?
  _curry1(function trim(str) {
    var beginRx = new RegExp('^[' + ws + '][' + ws + ']*');
    var endRx = new RegExp('[' + ws + '][' + ws + ']*$');
    return str.replace(beginRx, '').replace(endRx, '');
  }) :
  _curry1(function trim(str) {
    return str.trim();
  });

  var union =
  _curry2(
  compose(uniq, _concat));

  var unnest =
  chain(_identity);

  var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

  function unwrapExports (x) {
  	return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, 'default') ? x['default'] : x;
  }

  function createCommonjsModule(fn, module) {
  	return module = { exports: {} }, fn(module, module.exports), module.exports;
  }

  var PLACEHOLDER = "🍛";
  var $ = PLACEHOLDER;
  var bindInternal3 = function bindInternal3(func, thisContext) {
    return function (a, b, c) {
      return func.call(thisContext, a, b, c);
    };
  };
  var some$1 = function fastSome(subject, fn, thisContext) {
    var length = subject.length,
        iterator = thisContext !== undefined ? bindInternal3(fn, thisContext) : fn,
        i;
    for (i = 0; i < length; i++) {
      if (iterator(subject[i], i, subject)) {
        return true;
      }
    }
    return false;
  };
  var curry$1 = function curry(fn) {
    var test = function test(x) {
      return x === PLACEHOLDER;
    };
    return function curried() {
      var arguments$1 = arguments;
      var argLength = arguments.length;
      var args = new Array(argLength);
      for (var i = 0; i < argLength; ++i) {
        args[i] = arguments$1[i];
      }
      var countNonPlaceholders = function countNonPlaceholders(toCount) {
        var count = toCount.length;
        while (!test(toCount[count])) {
          count--;
        }
        return count;
      };
      var length = some$1(args, test) ? countNonPlaceholders(args) : args.length;
      function saucy() {
        var arguments$1 = arguments;
        var arg2Length = arguments.length;
        var args2 = new Array(arg2Length);
        for (var j = 0; j < arg2Length; ++j) {
          args2[j] = arguments$1[j];
        }
        return curried.apply(this, args.map(function (y) {
          return test(y) && args2[0] ? args2.shift() : y;
        }).concat(args2));
      }
      return length >= fn.length ? fn.apply(this, args) : saucy;
    };
  };
  var innerpipe = function innerpipe(args) {
    return function (x) {
      var first = args[0];
      var rest = args.slice(1);
      var current = first(x);
      for (var a = 0; a < rest.length; a++) {
        current = rest[a](current);
      }
      return current;
    };
  };
  function pipe$1() {
    var arguments$1 = arguments;
    var argLength = arguments.length;
    var args = new Array(argLength);
    for (var i = 0; i < argLength; ++i) {
      args[i] = arguments$1[i];
    }
    return innerpipe(args);
  }
  var prop$1 = curry$1(function (property, o) {
    return o && property && o[property];
  });
  var _keys = Object.keys;
  var keys$1 = _keys;
  var propLength = prop$1("length");
  var objectLength = pipe$1(keys$1, propLength);
  var length$1 = function length(x) {
    return _typeof(x) === "object" ? objectLength(x) : propLength(x);
  };
  var delegatee = curry$1(function (method, arg, x) {
    return x[method](arg);
  });
  var filter$1 = delegatee("filter");
  var flipIncludes = curry$1(function (list, x) {
    return list.includes(x);
  });
  var matchingKeys = curry$1(function (list, o) {
    return filter$1(flipIncludes(list), keys$1(o));
  });
  var matchingKeyCount = curry$1(function (list, o) {
    return pipe$1(matchingKeys(list), length$1)(o);
  });
  var expectKArgs = function expectKArgs(expected, args) {
    return matchingKeyCount(expected, args) >= Object.keys(expected).length;
  };
  var curryObjectK = curry$1(function (keys, fn) {
    return function λcurryObjectK(args) {
      var includes = function includes(y) {
        return keys.includes(y);
      };
      return Object.keys(args).filter(includes).length === keys.length ? fn(args) : function (z) {
        return λcurryObjectK(Object.assign({}, args, z));
      };
    };
  });
  function curryObjectN(arity, fn) {
    return function λcurryObjectN(args) {
      var joined = function joined(z) {
        return λcurryObjectN(Object.assign({}, args, z));
      };
      return args && Object.keys(args).length >= arity ? fn(args) : joined;
    };
  }
  function curryObjectKN(ref, fn) {
    var k = ref.k;
    var n = ref.n;
    return function λcurryObjectKN(args) {
      var joined = function joined(z) {
        return λcurryObjectKN(Object.assign({}, args, z));
      };
      return expectKArgs(k, args) || Object.keys(args).length >= n ? fn(args) : joined;
    };
  }
  var curryify = function curryify(test) {
    if (typeof test !== "function") {
      throw new TypeError("Expected to be given a function to test placeholders!");
    }
    return function (fn) {
      if (typeof fn !== "function") {
        throw new TypeError("Expected to be given a function to curry!");
      }
      return function curried() {
        var arguments$1 = arguments;
        var argLength = arguments.length;
        var args = new Array(argLength);
        for (var i = 0; i < argLength; ++i) {
          args[i] = arguments$1[i];
        }
        var countNonPlaceholders = function countNonPlaceholders(toCount) {
          var count = toCount.length;
          while (!test(toCount[count])) {
            count--;
          }
          return count;
        };
        var length = some$1(args, test) ? countNonPlaceholders(args) : args.length;
        return length >= fn.length ? fn.apply(this, args) : function saucy() {
          var arguments$1 = arguments;
          var arg2Length = arguments.length;
          var args2 = new Array(arg2Length);
          for (var j = 0; j < arg2Length; ++j) {
            args2[j] = arguments$1[j];
          }
          return curried.apply(this, args.map(function (y) {
            return test(y) && args2[0] ? args2.shift() : y;
          }).concat(args2));
        };
      };
    };
  };
  var K = function K(x) {
    return function () {
      return x;
    };
  };
  var I = function I(x) {
    return x;
  };
  var remapParameters = function remapParameters(indices, arr) {
    var copy = Array.from(arr);
    if (!copy.length) {
      return copy;
    }
    return copy.map(function (x, index) {
      if (indices.includes(index)) {
        return copy[indices[index]];
      }
      return x;
    });
  };
  var remapArray = curry$1(remapParameters);
  var remapFunction = function remapFunction(indices, fn) {
    var remapArgs = remapArray(indices);
    var curried = curry$1(fn);
    return function remappedFn() {
      var args = remapArgs(Array.from(arguments));
      return curried.apply(null, args);
    };
  };
  var remap = curry$1(remapFunction);

  var entrust0 = function entrust0(fn, x) {
    return x[fn]();
  };
  var e0 = curry$1(entrust0);
  var entrust1 = function entrust1(fn, a, x) {
    return x[fn](a);
  };
  var e1 = curry$1(entrust1);
  var entrust2 = function entrust2(fn, a, b, x) {
    return x[fn](a, b);
  };
  var e2 = curry$1(entrust2);

  var flatten$1 = function flatten(a) {
    return a.reduce(function (x, y) {
      return x.concat(y);
    });
  };
  var flatMap = function flatMap(a
  , f
  ) {
    return !f ? flatten$1(a) : flatten$1(a.map(f));
  };
  var flatmapFast = flatMap;

  var version$1 = "3.6.0";
  var random = function random(x) {
    if (x === void 0) x = 1;
    return Math.round(Math.random() * x);
  };
  var floor = function floor(x) {
    return Math.floor(Math.random() * x);
  };
  var floorMin = curry$1(function (min, x) {
    return floor(x) + min;
  });
  var f =
  Object.freeze({
    floor: floor,
    floorMin: floorMin
  });
  var __iterate = function __iterate(total, fn) {
    var count = total;
    var agg = [];
    if (typeof fn !== "function" || typeof count !== "number") {
      return agg;
    }
    while (count > 0) {
      count--;
      agg.push(fn());
    }
    return agg;
  };
  var iterate = curry$1(__iterate);
  var keys$2 = Object.keys;
  var take$1 = curry$1(function (encase, o) {
    var obj;
    if (o && o[0] && o.length) {
      var found = floor(o.length);
      var selection = o[found];
      return !encase ? selection : [selection];
    }
    var ks = keys$2(o);
    var index = floor(ks.length);
    var key = ks[index];
    var value = o[key];
    if (encase) {
      return obj = {}, obj[key] = value, obj;
    }
    return value;
  });
  var pick$1 = take$1(false);
  var grab = take$1(true);
  var allot = curry$1(function (howMany, ofThing) {
    return iterate(howMany, function () {
      return pick$1(ofThing);
    });
  });
  var t =
  Object.freeze({
    take: take$1,
    pick: pick$1,
    grab: grab,
    allot: allot
  });
  var bindInternal3$1 = function bindInternal3(func, thisContext) {
    return function (a, b, c) {
      return func.call(thisContext, a, b, c);
    };
  };
  var filter$2 = function fastFilter(subject, fn, thisContext) {
    var length = subject.length,
        result = [],
        iterator = thisContext !== undefined ? bindInternal3$1(fn, thisContext) : fn,
        i;
    for (i = 0; i < length; i++) {
      if (iterator(subject[i], i, subject)) {
        result.push(subject[i]);
      }
    }
    return result;
  };
  var filter$1$1 = function fastFilterObject(subject, fn, thisContext) {
    var keys = Object.keys(subject),
        length = keys.length,
        result = {},
        iterator = thisContext !== undefined ? bindInternal3$1(fn, thisContext) : fn,
        i,
        key;
    for (i = 0; i < length; i++) {
      key = keys[i];
      if (iterator(subject[key], key, subject)) {
        result[key] = subject[key];
      }
    }
    return result;
  };
  var filter$2$1 = function fastFilter(subject, fn, thisContext) {
    if (subject instanceof Array) {
      return filter$2(subject, fn, thisContext);
    } else {
      return filter$1$1(subject, fn, thisContext);
    }
  };
  var has$1 = function has(x, y) {
    return !!y[x];
  };
  var isArray = Array.isArray;
  var __willDelegate = function __willDelegate(method, functor) {
    return has$1(method, functor) && !isArray(functor);
  };
  function __delegateFastBinary(method, fast, fn, functor) {
    return __willDelegate(method, functor) ? functor[method](fn) : fast(functor, fn);
  }
  var delegateFastBinary = curry$1(__delegateFastBinary);
  function __delegateFastTertiary(method, fast, fn, initial, functor) {
    return __willDelegate(method, functor) ? functor[method](fn, initial) : fast(functor, fn, initial);
  }
  var delegateFastTertiary = curry$1(__delegateFastTertiary);
  var filter$3 = delegateFastBinary("filter", filter$2$1);
  var join$1 = e1("join");
  var concat$1 = curry$1(function (a, b) {
    return a.concat(b);
  });
  var __sort = function __sort(fn, functor) {
    var copy = Array.from(functor);
    copy.sort(fn);
    return copy;
  };
  var sort$1 = curry$1(__sort);
  var __difference = function __difference(bList, aList) {
    return filter$3(function (x) {
      return !aList.includes(x);
    }, bList);
  };
  var difference$1 = curry$1(__difference);
  var __symmetricDifference = function __symmetricDifference(a, b) {
    var ab = difference$1(a, b);
    var ba = difference$1(b, a);
    return ab.concat(ba);
  };
  var symmetricDifference$1 = curry$1(__symmetricDifference);
  var __relativeIndex = function __relativeIndex(length, index) {
    return index > -1 ? index : length - Math.abs(index);
  };
  var relativeIndex = curry$1(__relativeIndex);
  var __alterIndex = function __alterIndex(index, fn, input) {
    var i = relativeIndex(input.length, index);
    var copy = [].concat(input);
    copy[i] = fn(copy[i]);
    return copy;
  };
  var alterIndex = curry$1(__alterIndex);
  var alterFirstIndex = alterIndex(0);
  var alterLastIndex = alterIndex(-1);
  var alphabet = "abcdefghijklmnopqrstuvwxyz".split("");
  var wordSource = curry$1(function (source, howLong) {
    return pipe$1(allot(howLong), join$1(""))(source);
  });
  var word = function word(x) {
    if (x === void 0) x = 5;
    return wordSource(alphabet, x);
  };
  var w =
  Object.freeze({
    wordSource: wordSource,
    word: word
  });
  var shuffle = function shuffle(list) {
    var newList = [].concat(list);
    var start = newList.length;
    while (start-- > 0) {
      var index = Math.floor(Math.random() * start + 1);
      var current = newList[index];
      var newer = newList[start];
      newList[index] = newer;
      newList[start] = current;
    }
    return newList;
  };
  var s =
  Object.freeze({
    shuffle: shuffle
  });
  var __choice = function __choice(cnFn, b, a) {
    return cnFn(a, b) ? a : b;
  };
  var choice = curry$1(__choice);
  var flip$1 = function flip(fn) {
    return curry$1(function (a, b) {
      return fn(b, a);
    });
  };
  var fork = e2("fork");
  var map$1 = function fastMap(subject, fn, thisContext) {
    var length = subject.length,
        result = new Array(length),
        iterator = thisContext !== undefined ? bindInternal3$1(fn, thisContext) : fn,
        i;
    for (i = 0; i < length; i++) {
      result[i] = iterator(subject[i], i, subject);
    }
    return result;
  };
  var map$1$1 = function fastMapObject(subject, fn, thisContext) {
    var keys = Object.keys(subject),
        length = keys.length,
        result = {},
        iterator = thisContext !== undefined ? bindInternal3$1(fn, thisContext) : fn,
        i,
        key;
    for (i = 0; i < length; i++) {
      key = keys[i];
      result[key] = iterator(subject[key], key, subject);
    }
    return result;
  };
  var map$2 = function fastMap(subject, fn, thisContext) {
    if (subject instanceof Array) {
      return map$1(subject, fn, thisContext);
    } else {
      return map$1$1(subject, fn, thisContext);
    }
  };
  var __map = function __map(fn, functor) {
    if (functor && !Array.isArray(functor) && functor.map) {
      return functor.map(fn);
    }
    return map$2(functor, fn);
  };
  var map$3 = curry$1(__map);
  var __isTypeof = function __isTypeof(type, x) {
    return type === _typeof(x);
  };
  var isTypeof = curry$1(__isTypeof);
  var isBoolean = isTypeof("boolean");
  var isNumber = isTypeof("number");
  var isFunction = isTypeof("function");
  var isString = isTypeof("string");
  var isNil$1 = function isNil(x) {
    return x == null;
  };
  var isObject = isTypeof("object");
  var isArray$1 = Array.isArray;
  var isDistinctObject = function isDistinctObject(x) {
    return !isNil$1(x) && isObject(x) && !isArray$1(x);
  };
  var isPOJO = isDistinctObject;
  var bindInternal4 = function bindInternal4(func, thisContext) {
    return function (a, b, c, d) {
      return func.call(thisContext, a, b, c, d);
    };
  };
  var reduce$1 = function fastReduce(subject, fn, initialValue, thisContext) {
    var length = subject.length,
        iterator = thisContext !== undefined ? bindInternal4(fn, thisContext) : fn,
        i,
        result;
    if (initialValue === undefined) {
      i = 1;
      result = subject[0];
    } else {
      i = 0;
      result = initialValue;
    }
    for (; i < length; i++) {
      result = iterator(result, subject[i], i, subject);
    }
    return result;
  };
  var reduce$1$1 = function fastReduceObject(subject, fn, initialValue, thisContext) {
    var keys = Object.keys(subject),
        length = keys.length,
        iterator = thisContext !== undefined ? bindInternal4(fn, thisContext) : fn,
        i,
        key,
        result;
    if (initialValue === undefined) {
      i = 1;
      result = subject[keys[0]];
    } else {
      i = 0;
      result = initialValue;
    }
    for (; i < length; i++) {
      key = keys[i];
      result = iterator(result, subject[key], key, subject);
    }
    return result;
  };
  var reduce$2 = function fastReduce(subject, fn, initialValue, thisContext) {
    if (subject instanceof Array) {
      return reduce$1(subject, fn, initialValue, thisContext);
    } else {
      return reduce$1$1(subject, fn, initialValue, thisContext);
    }
  };
  var reduce$3 = delegateFastTertiary("reduce", reduce$2);
  var __ap = function __ap(applicative, functor) {
    if (functor && functor.ap && isFunction(functor.ap)) {
      return functor.ap(applicative);
    }
    if (isFunction(functor)) {
      return function (x) {
        return applicative(x)(functor(x));
      };
    }
    return reduce$3(function (agg, f) {
      return agg.concat(map$3(f, functor));
    }, [], applicative);
  };
  var ap$1 = curry$1(__ap);
  var fold = e2("fold");
  var chain$1 = delegateFastBinary("chain", flatmapFast);
  var flatMap$1 = chain$1;
  var __equals = function __equals(a, b) {
    return a === b;
  };
  var equals$1 = curry$1(__equals);
  var equal = equals$1;
  var round = Math.round;
  var __add = function __add(a, b) {
    return b + a;
  };
  var add$1 = curry$1(__add);
  var __subtract = function __subtract(a, b) {
    return a - b;
  };
  var subtract$1 = curry$1(__subtract);
  var __multiply = function __multiply(a, b) {
    return b * a;
  };
  var multiply$1 = curry$1(__multiply);
  var __divide = function __divide(a, b) {
    return a / b;
  };
  var divide$1 = curry$1(__divide);
  var __pow = function __pow(a, b) {
    return Math.pow(b, a);
  };
  var pow = curry$1(__pow);
  var not$1 = function not(x) {
    return !x;
  };
  var invert$1 = function invert(x) {
    return Object.keys(x).reduce(function (o, key) {
      var value = x[key];
      o[value] = o[value] ? o[value].concat(key) : [key];
      return o;
    }, {});
  };
  var __reject = function __reject(fn, o) {
    return filter$3(function (x) {
      return !fn(x);
    }, o);
  };
  var reject$1 = curry$1(__reject);
  var trim$1 = e0("trim");
  var charAt = e1("charAt");
  var codePointAt = e1("codePointAt");
  var match$1 = curry$1(function (a, b) {
    var z = b.match(a);
    return z === null ? [] : z;
  });
  var repeat$1 = curry$1(function (x, n) {
    var output = new Array(n);
    for (var i = 0; i < n; i++) {
      output[i] = x;
    }
    return output;
  });
  var search = e1("search");
  var split$1 = e1("split");
  var endsWithLength = e2("endsWith");
  var __endsWith = function __endsWith(x, i) {
    var last = i[i.length - 1];
    return Array.isArray(x) ? last === x[0] : last === x;
  };
  var endsWith$1 = curry$1(__endsWith);
  var indexOfFromIndex = e2("indexOf");
  var __indexOf = function __indexOf(toSearch, x) {
    return indexOfFromIndex(toSearch, 0, x);
  };
  var indexOf$1 = curry$1(__indexOf);
  var lastIndexOfFromIndex = e2("lastIndexOf");
  var __lastIndexOf = function __lastIndexOf(toSearch, x) {
    return lastIndexOfFromIndex(toSearch, Infinity, x);
  };
  var lastIndexOf$1 = curry$1(__lastIndexOf);
  var padEnd = e2("padEnd");
  var padStart = e2("padStart");
  var replace$1 = e2("replace");
  var startsWithFromPosition = e2("startsWith");
  var __startsWith = function __startsWith(x, i) {
    var first = i[0];
    return Array.isArray(x) ? first === x[0] : first === x;
  };
  var startsWith$1 = curry$1(__startsWith);
  var substr = e2("substr");
  var __ternary = function __ternary(cn, b, a) {
    return cn ? a : b;
  };
  var ternary = curry$1(__ternary);
  var __triplet = function __triplet(cnFn, bFn, aFn, o) {
    return cnFn(o) ? aFn(o) : bFn(o);
  };
  var triplet = curry$1(__triplet);
  var __range = function __range(start, end) {
    var agg = [];
    var swap = start < end;
    var ref = swap ? [start, end] : [end + 1, start + 1];
    var a = ref[0];
    var b = ref[1];
    for (var x = a; x < b; x++) {
      agg.push(x);
    }
    return swap ? agg : agg.reverse();
  };
  var range$1 = curry$1(__range);
  var _keys$1 = Object.keys;
  var _freeze = Object.freeze;
  var _assign$1 = Object.assign;
  var keys$1$1 = _keys$1;
  var freeze = _freeze;
  var assign$1 = _assign$1;
  var entries = function entries(o) {
    return pipe$1(keys$1$1, map$3(function (k) {
      return [k, o[k]];
    }))(o);
  };
  var toPairs$1 = entries;
  var fromPairs$1 = reduce$3(function (agg, ref) {
    var obj;
    var k = ref[0];
    var v = ref[1];
    return merge$2(agg, (obj = {}, obj[k] = v, obj));
  }, {});
  var __pairwise = function __pairwise(hoc, fn, o) {
    return pipe$1(toPairs$1, hoc(fn))(o);
  };
  var pairwise = curry$1(__pairwise);
  var __pairwiseObject = function __pairwiseObject(hoc, fn, o) {
    return pipe$1(pairwise(hoc, fn), fromPairs$1)(o);
  };
  var pairwiseObject = curry$1(__pairwiseObject);
  var mapTuples = pairwiseObject(map$3);
  var mapTuple = mapTuples;
  var __mapKeys = function __mapKeys(fn, o) {
    return mapTuples(function (ref) {
      var k = ref[0];
      var v = ref[1];
      return [fn(k), v];
    }, o);
  };
  var mapKeys = curry$1(__mapKeys);
  var __merge = function __merge(a, b) {
    return assign$1({}, a, b);
  };
  var merge$2 = curry$1(__merge);
  var __pathOr = function __pathOr(def, lenses, input) {
    return reduce$3(function (focus, lens) {
      return focus[lens] || def;
    }, input, lenses);
  };
  var pathOr$1 = curry$1(__pathOr);
  var __pathSatisfies = function __pathSatisfies(equiv, pathTo, input) {
    return pipe$1(path$1(pathTo), equiv, Boolean)(input);
  };
  var pathSatisfies$1 = curry$1(__pathSatisfies);
  var __propSatisfies = function __propSatisfies(equiv, propTo, input) {
    return pipe$1(prop$2(propTo), equiv, Boolean)(input);
  };
  var propSatisfies$1 = curry$1(__propSatisfies);
  var path$1 = pathOr$1(null);
  var __propOr = function __propOr(def, property, input) {
    return pathOr$1(def, [property], input);
  };
  var propOr$1 = curry$1(__propOr);
  var prop$2 = propOr$1(null);
  var __pathEq = function __pathEq(lenses, equiv, input) {
    return pathSatisfies$1(equals$1(equiv), lenses, input);
  };
  var pathEq$1 = curry$1(__pathEq);
  var __propIs = function __propIs(type, property, input) {
    return pipe$1(prop$2(property), function (val) {
      return val != null && val.constructor === type || val instanceof type;
    }, Boolean)(input);
  };
  var propIs$1 = curry$1(__propIs);
  var __propEq = function __propEq(property, equiv, input) {
    return pathSatisfies$1(equals$1(equiv), [property], input);
  };
  var propEq$1 = curry$1(__propEq);
  var propLength$1 = prop$2("length");
  var objectLength$1 = pipe$1(keys$1$1, propLength$1);
  var length$2 = propLength$1;
  var anyLength = function anyLength(x) {
    return _typeof(x) === "object" ? objectLength$1(x) : propLength$1(x);
  };
  var some = function fastSome(subject, fn, thisContext) {
    var length = subject.length,
        iterator = thisContext !== undefined ? bindInternal3$1(fn, thisContext) : fn,
        i;
    for (i = 0; i < length; i++) {
      if (iterator(subject[i], i, subject)) {
        return true;
      }
    }
    return false;
  };
  var every = function fastEvery(subject, fn, thisContext) {
    var length = subject.length,
        iterator = thisContext !== undefined ? bindInternal3$1(fn, thisContext) : fn,
        i;
    for (i = 0; i < length; i++) {
      if (!iterator(subject[i], i, subject)) {
        return false;
      }
    }
    return true;
  };
  var keys$2$1 = Object.keys;
  var __which = function __which(compare, fn, o) {
    var arecomp = flip$1(compare);
    return triplet(Array.isArray, arecomp(fn), pipe$1(keys$2$1, arecomp(function (key) {
      return fn(o[key], key);
    })), o);
  };
  var which = curry$1(__which);
  var some$1$1 = which(some);
  var every$1 = which(every);
  var innerpipe$1 = function innerpipe(args) {
    return function (x) {
      var first = args[0];
      var rest = args.slice(1);
      var current = first(x);
      for (var a = 0; a < rest.length; a++) {
        current = rest[a](current);
      }
      return current;
    };
  };
  function compose$2() {
    var arguments$1 = arguments;
    var length = arguments.length;
    var args = new Array(length);
    for (var i = length - 1; i > -1; --i) {
      args[i] = arguments$1[i];
    }
    return innerpipe$1(args.reverse());
  }
  var version$1$1 = version$1;
  var random$1 = Object.assign(random, f, t, w, s);

  var fUtility_es = /*#__PURE__*/Object.freeze({
    compose: compose$2,
    version: version$1$1,
    random: random$1,
    concat: concat$1,
    join: join$1,
    sort: sort$1,
    symmetricDifference: symmetricDifference$1,
    difference: difference$1,
    alterIndex: alterIndex,
    alterFirstIndex: alterFirstIndex,
    alterLastIndex: alterLastIndex,
    relativeIndex: relativeIndex,
    choice: choice,
    filter: filter$3,
    flip: flip$1,
    fork: fork,
    iterate: iterate,
    map: map$3,
    ap: ap$1,
    fold: fold,
    chain: chain$1,
    flatMap: flatMap$1,
    equals: equals$1,
    equal: equal,
    round: round,
    add: add$1,
    subtract: subtract$1,
    divide: divide$1,
    multiply: multiply$1,
    pow: pow,
    invert: invert$1,
    not: not$1,
    reduce: reduce$3,
    reject: reject$1,
    charAt: charAt,
    codePointAt: codePointAt,
    endsWith: endsWith$1,
    indexOf: indexOf$1,
    lastIndexOf: lastIndexOf$1,
    match: match$1,
    padEnd: padEnd,
    padStart: padStart,
    repeat: repeat$1,
    replace: replace$1,
    search: search,
    split: split$1,
    startsWith: startsWith$1,
    substr: substr,
    trim: trim$1,
    ternary: ternary,
    triplet: triplet,
    range: range$1,
    keys: keys$1$1,
    assign: assign$1,
    freeze: freeze,
    merge: merge$2,
    entries: entries,
    fromPairs: fromPairs$1,
    toPairs: toPairs$1,
    mapTuple: mapTuple,
    mapTuples: mapTuples,
    mapKeys: mapKeys,
    pairwise: pairwise,
    pairwiseObject: pairwiseObject,
    path: path$1,
    pathOr: pathOr$1,
    pathEq: pathEq$1,
    pathSatisfies: pathSatisfies$1,
    prop: prop$2,
    propOr: propOr$1,
    propIs: propIs$1,
    propEq: propEq$1,
    propSatisfies: propSatisfies$1,
    isTypeof: isTypeof,
    isBoolean: isBoolean,
    isNumber: isNumber,
    isFunction: isFunction,
    isString: isString,
    isObject: isObject,
    isNil: isNil$1,
    isArray: isArray$1,
    isDistinctObject: isDistinctObject,
    isPOJO: isPOJO,
    length: length$2,
    objectLength: objectLength$1,
    anyLength: anyLength,
    which: which,
    some: some$1$1,
    every: every$1,
    pipe: pipe$1,
    $: $,
    PLACEHOLDER: PLACEHOLDER,
    curryify: curryify,
    curry: curry$1,
    curryObjectK: curryObjectK,
    curryObjectN: curryObjectN,
    curryObjectKN: curryObjectKN,
    remap: remap,
    remapArray: remapArray,
    K: K,
    I: I
  });

  var daggy = createCommonjsModule(function (module, exports) {
    (function (global, factory) {
      {
        factory(exports);
      }
    })(commonjsGlobal, function (exports) {
      function create(proto) {
        function Ctor() {}
        Ctor.prototype = proto;
        return new Ctor();
      }
      exports.create = create;
      function getInstance(self, constructor) {
        return self instanceof constructor ? self : create(constructor.prototype);
      }
      exports.getInstance = getInstance;
      function tagged() {
        var fields = [].slice.apply(arguments);
        function wrapped() {
          var self = getInstance(this, wrapped),
              i;
          if (arguments.length != fields.length) throw new TypeError('Expected ' + fields.length + ' arguments, got ' + arguments.length);
          for (i = 0; i < fields.length; i++) {
            self[fields[i]] = arguments[i];
          }
          return self;
        }
        wrapped._length = fields.length;
        return wrapped;
      }
      exports.tagged = tagged;
      function taggedSum(constructors) {
        var key;
        function definitions() {
          throw new TypeError('Tagged sum was called instead of one of its properties.');
        }
        function makeCata(key) {
          return function (dispatches) {
            var fields = constructors[key],
                args = [],
                i;
            if (!dispatches[key]) throw new TypeError("Constructors given to cata didn't include: " + key);
            for (i = 0; i < fields.length; i++) {
              args.push(this[fields[i]]);
            }
            return dispatches[key].apply(this, args);
          };
        }
        function makeProto(key) {
          var proto = create(definitions.prototype);
          proto.cata = makeCata(key);
          return proto;
        }
        for (key in constructors) {
          if (!constructors[key].length) {
            definitions[key] = makeProto(key);
            continue;
          }
          definitions[key] = tagged.apply(null, constructors[key]);
          definitions[key].prototype = makeProto(key);
        }
        return definitions;
      }
      exports.taggedSum = taggedSum;
    });
  });

  var combinators = createCommonjsModule(function (module) {
    function apply(f) {
      return function (x) {
        return f(x);
      };
    }
    function compose(f) {
      return function (g) {
        return function (x) {
          return f(g(x));
        };
      };
    }
    function constant(a) {
      return function (b) {
        return a;
      };
    }
    function fix(f) {
      function g(h) {
        return function (x) {
          return f(h(h))(x);
        };
      }
      return g(g);
    }
    function flip(f) {
      return function (a) {
        return function (b) {
          return f(b)(a);
        };
      };
    }
    function identity(a) {
      return a;
    }
    function substitution(f) {
      return function (g) {
        return function (x) {
          f(x)(g(x));
        };
      };
    }
    function thrush(x) {
      return function (f) {
        return f(x);
      };
    }
    module.exports = {
      apply: apply,
      compose: compose,
      constant: constant,
      fix: fix,
      flip: flip,
      identity: identity,
      substitution: substitution,
      thrush: thrush
    };
  });
  var combinators_1 = combinators.apply;
  var combinators_2 = combinators.compose;
  var combinators_3 = combinators.constant;
  var combinators_4 = combinators.fix;
  var combinators_5 = combinators.flip;
  var combinators_6 = combinators.identity;
  var combinators_7 = combinators.substitution;
  var combinators_8 = combinators.thrush;

  var either$1 = createCommonjsModule(function (module) {
    var compose = combinators.compose,
        identity = combinators.identity,
        Either = daggy.taggedSum({
      Left: ['l'],
      Right: ['r']
    });
    Either.prototype.fold = function (f, g) {
      return this.cata({
        Left: f,
        Right: g
      });
    };
    Either.of = Either.Right;
    Either.prototype.swap = function () {
      return this.fold(function (l) {
        return Either.Right(l);
      }, function (r) {
        return Either.Left(r);
      });
    };
    Either.prototype.bimap = function (f, g) {
      return this.fold(function (l) {
        return Either.Left(f(l));
      }, function (r) {
        return Either.Right(g(r));
      });
    };
    Either.prototype.chain = function (f) {
      return this.fold(function (l) {
        return Either.Left(l);
      }, f);
    };
    Either.prototype.concat = function (b) {
      return this.fold(function (l) {
        return Either.Left(l);
      }, function (r) {
        return b.chain(function (t) {
          return Either.Right(r.concat(t));
        });
      });
    };
    Either.prototype.map = function (f) {
      return this.chain(function (a) {
        return Either.of(f(a));
      });
    };
    Either.prototype.ap = function (a) {
      return this.chain(function (f) {
        return a.map(f);
      });
    };
    Either.prototype.sequence = function (p) {
      return this.traverse(identity, p);
    };
    Either.prototype.traverse = function (f, p) {
      return this.cata({
        Left: function Left(l) {
          return p.of(Either.Left(l));
        },
        Right: function Right(r) {
          return f(r).map(Either.Right);
        }
      });
    };
    Either.EitherT = function (M) {
      var EitherT = daggy.tagged('run');
      EitherT.prototype.fold = function (f, g) {
        return this.run.chain(function (o) {
          return M.of(o.fold(f, g));
        });
      };
      EitherT.of = function (x) {
        return EitherT(M.of(Either.Right(x)));
      };
      EitherT.prototype.swap = function () {
        return this.fold(function (l) {
          return Either.Right(l);
        }, function (r) {
          return Either.Left(r);
        });
      };
      EitherT.prototype.bimap = function (f, g) {
        return this.fold(function (l) {
          return Either.Left(f(l));
        }, function (r) {
          return Either.Right(g(r));
        });
      };
      EitherT.prototype.chain = function (f) {
        var m = this.run;
        return EitherT(m.chain(function (o) {
          return o.fold(function (a) {
            return M.of(Either.Left(a));
          }, function (a) {
            return f(a).run;
          });
        }));
      };
      EitherT.prototype.map = function (f) {
        return this.chain(function (a) {
          return EitherT.of(f(a));
        });
      };
      EitherT.prototype.ap = function (a) {
        return this.chain(function (f) {
          return a.map(f);
        });
      };
      return EitherT;
    };
    module.exports = Either;
  });

  var fantasyEithers = createCommonjsModule(function (module) {
    module.exports = either$1;
  });

  var entrust = createCommonjsModule(function (module, exports) {
    (function (global, factory) {
      factory(exports);
    })(commonjsGlobal, function (exports) {
      var PLACEHOLDER = "\uD83C\uDF5B";
      var innerpipe = function innerpipe(b) {
        return function (c) {
          for (var d = b[0], e = b.slice(1), f = d(c), g = 0; g < e.length; g++) {
            f = e[g](f);
          }
          return f;
        };
      };
      function pipe() {
        for (var a = arguments, b = arguments.length, c = Array(b), d = 0; d < b; ++d) {
          c[d] = a[d];
        }
        return innerpipe(c);
      }
      var bindInternal3 = function bindInternal3(d, e) {
        return function (f, a, b) {
          return d.call(e, f, a, b);
        };
      };
      var some$1 = function some$1(a, b, c) {
        var d,
            e = a.length,
            f = c === void 0 ? b : bindInternal3(b, c);
        for (d = 0; d < e; d++) {
          if (f(a[d], d, a)) {
            return !0;
          }
        }
        return !1;
      };
      var curry = function curry(a) {
        var b = function b(a) {
          return a === PLACEHOLDER;
        };
        return function c() {
          for (var d = arguments, e = arguments.length, f = Array(e), g = 0; g < e; ++g) {
            f[g] = d[g];
          }
          var h = some$1(f, b) ? function (a) {
            for (var c = a.length; !b(a[c]);) {
              c--;
            }
            return c;
          }(f) : f.length;
          return h >= a.length ? a.apply(this, f) : function () {
            for (var a = arguments, d = arguments.length, e = Array(d), g = 0; g < d; ++g) {
              e[g] = a[g];
            }
            return c.apply(this, f.map(function (a) {
              return b(a) && e[0] ? e.shift() : a;
            }).concat(e));
          };
        };
      };
      var prop = curry(function (a, b) {
        return b && a && b[a];
      });
      var _keys = Object.keys;
      var keys = _keys;
      var propLength = prop("length");
      var objectLength = pipe(keys, propLength);
      var delegatee = curry(function (a, b, c) {
        return c[a](b);
      });
      var filter = delegatee("filter");
      var entrust0 = function entrust0(fn, x) {
        return x[fn]();
      };
      var e0 = curry(entrust0);
      var entrust1 = function entrust1(fn, a, x) {
        return x[fn](a);
      };
      var e1 = curry(entrust1);
      var entrust2 = function entrust2(fn, a, b, x) {
        return x[fn](a, b);
      };
      var e2 = curry(entrust2);
      var entrust3 = function entrust3(fn, a, b, c, x) {
        return x[fn](a, b, c);
      };
      var e3 = curry(entrust3);
      var entrust4 = function entrust4(fn, a, b, c, d, x) {
        return x[fn](a, b, c, d);
      };
      var e4 = curry(entrust4);
      var entrust5 = function entrust5(fn, a, b, c, d, e, x) {
        return x[fn](a, b, c, d, e);
      };
      var e5 = curry(entrust5);
      var entrust6 = function entrust6(fn, a, b, c, d, e, f, x) {
        return x[fn](a, b, c, d, e, f);
      };
      var e6 = curry(entrust6);
      var entrust7 = function entrust7(fn, a, b, c, d, e, f, g, x) {
        return x[fn](a, b, c, d, e, f, g);
      };
      var e7 = curry(entrust7);
      var entrust8 = function entrust8(fn, a, b, c, d, e, f, g, h, x) {
        return x[fn](a, b, c, d, e, f, g, h);
      };
      var e8 = curry(entrust8);
      var entrust9 = function entrust9(fn, a, b, c, d, e, f, g, h, i, x) {
        return x[fn](a, b, c, d, e, f, g, h, i);
      };
      var e9 = curry(entrust9);
      var entrust10 = function entrust10(fn, a, b, c, d, e, f, g, h, i, j, x) {
        return x[fn](a, b, c, d, e, f, g, h, i, j);
      };
      var e10 = curry(entrust10);
      var entrustN = function entrustN(n, method, args, delegatee) {
        var entrustees = [e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10];
        var params = [method].concat(args, [delegatee]);
        return entrustees[n].apply(null, params);
      };
      var eN = curry(entrustN);
      function entrustD(n, m, a, d) {
        if (n !== a.length) {
          throw new Error(m + " expects total args (" + a.length + ") to equal the given arity (" + n + ")");
        }
        return entrustN(n, m, a, d);
      }
      var eD = curry(entrustD);
      var raw = {
        e0: entrust0,
        e1: entrust1,
        e2: entrust2,
        e3: entrust3,
        e4: entrust4,
        e5: entrust5,
        e6: entrust6,
        e7: entrust7,
        e8: entrust8,
        e9: entrust9,
        e10: entrust10,
        eD: entrustD,
        eN: entrustN
      };
      var custom = function custom(curry) {
        var merge = function merge(a, b) {
          return Object.assign({}, a, b);
        };
        return Object.keys(raw).map(function (k) {
          return obj = {}, obj[k] = curry(raw[k]), obj;
          var obj;
        }).reduce(merge, {});
      };
      exports.raw = raw;
      exports.custom = custom;
      exports.e0 = e0;
      exports.e1 = e1;
      exports.e2 = e2;
      exports.e3 = e3;
      exports.e4 = e4;
      exports.e5 = e5;
      exports.e6 = e6;
      exports.e7 = e7;
      exports.e8 = e8;
      exports.e9 = e9;
      exports.e10 = e10;
      exports.eN = eN;
      exports.eD = eD;
      Object.defineProperty(exports, '__esModule', {
        value: true
      });
    });
  });
  unwrapExports(entrust);

  var handrail_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, '__esModule', {
      value: !0
    });
    var isEither = function isEither(a) {
      return a && fUtility_es.isObject(a) && a.fold && fUtility_es.isFunction(a.fold);
    },
        guided = fUtility_es.curry(function (a, b) {
      return isEither(b) ? b : a(b);
    }),
        Left$1 = fantasyEithers.Left,
        GuidedLeft = guided(Left$1),
        Right$1 = fantasyEithers.Right,
        GuidedRight = guided(Right$1),
        plural = function plural(a) {
      return 1 < a.length ? 's' : '';
    },
        expectFunctionProps = fUtility_es.curry(function (a, b) {
      return new Error(a + ': Expected ' + b.join(', ') + ' to be function' + plural(b) + '.');
    }),
        rejectNonFunctions = fUtility_es.reject(fUtility_es.isFunction),
        safeRailInputs = fUtility_es.pipe(rejectNonFunctions, Object.keys),
        rail = fUtility_es.curry(function (a, b, c) {
      if (null == c) return GuidedLeft(new Error('rail: Expected to be given non-null input.'));
      var d = safeRailInputs({
        assertion: a,
        wrongPath: b
      });
      if (0 < d.length) return GuidedLeft(expectFunctionProps('rail', d));
      var e = a(c);
      return (e ? GuidedRight : fUtility_es.pipe(b, GuidedLeft))(c);
    }),
        multiRail = fUtility_es.curry(function (a, b, c) {
      return fUtility_es.chain(rail(a, b), c);
    }),
        keyLength = fUtility_es.pipe(fUtility_es.keys, fUtility_es.length),
        allPropsAreFunctions = fUtility_es.pipe(function (a) {
      return [a];
    }, fUtility_es.ap([fUtility_es.pipe(fUtility_es.filter(fUtility_es.isFunction), keyLength, Number), keyLength]), function (a) {
      var b = a[0],
          c = a[1];
      return b === c;
    }),
        safeWarn = fUtility_es.curry(function (a, b) {
      return fUtility_es.pipe(rejectNonFunctions, fUtility_es.keys, expectFunctionProps(a))(b);
    }),
        internalRailSafety = fUtility_es.curryObjectN(3, function (a) {
      return rail(fUtility_es.K(allPropsAreFunctions(a)), fUtility_es.K(safeWarn('handrail', a)));
    }),
        handrail = fUtility_es.curry(function (a, b, c, d) {
      return fUtility_es.pipe(internalRailSafety({
        assertion: a,
        wrongPath: b,
        rightPath: c
      }), multiRail(a, b), fUtility_es.map(c))(d);
    }),
        guideRail = fUtility_es.curry(function (a, b, c) {
      var d = a[0],
          e = a.slice(1),
          f = d[0],
          g = d[1];
      return fUtility_es.pipe.apply(void 0, [rail(f, g)].concat(fUtility_es.map(function (b) {
        var c = b[0],
            a = b[1];
        return multiRail(c, a);
      }, e), [fUtility_es.map(b)]))(c);
    }),
        ap$1 = entrust.e1('ap'),
        bimap = entrust.e2('bimap'),
        fold = entrust.e2('fold'),
        map$1 = fUtility_es.map,
        chain$1 = fUtility_es.chain;
    exports.map = map$1, exports.chain = chain$1, exports.handrail = handrail, exports.baluster = rail, exports.balustrade = handrail, exports.net = fold, exports.rail = rail, exports.multiRail = multiRail, exports.guideRail = guideRail, exports.ap = ap$1, exports.isEither = isEither, exports.bimap = bimap, exports.fold = fold, exports.Left = Left$1, exports.GuidedLeft = GuidedLeft, exports.Right = Right$1, exports.GuidedRight = GuidedRight, exports.guided = guided;
  });
  unwrapExports(handrail_1);
  var handrail_2 = handrail_1.map;
  var handrail_3 = handrail_1.chain;
  var handrail_4 = handrail_1.handrail;
  var handrail_5 = handrail_1.baluster;
  var handrail_6 = handrail_1.balustrade;
  var handrail_7 = handrail_1.net;
  var handrail_8 = handrail_1.rail;
  var handrail_9 = handrail_1.multiRail;
  var handrail_10 = handrail_1.guideRail;
  var handrail_11 = handrail_1.ap;
  var handrail_12 = handrail_1.isEither;
  var handrail_13 = handrail_1.bimap;
  var handrail_14 = handrail_1.fold;
  var handrail_15 = handrail_1.Left;
  var handrail_16 = handrail_1.GuidedLeft;
  var handrail_17 = handrail_1.Right;
  var handrail_18 = handrail_1.GuidedRight;
  var handrail_19 = handrail_1.guided;

  var I$1 = function I(x) {
    return x;
  };

  var K$1 = function K(x) {
    return function () {
      return x;
    };
  };

  var ERRORS = Object.freeze({
    NOT_A_NUMBER: "Expected input to be a number",
    NOT_A_STRING: "Expected input to be a string"
  });

  var isType = curry(function (x, y) {
    return _typeof(y) === x;
  });
  var isNumber$1 = isType("number");
  var isString$1 = isType("string");

  var handleErrors = function handleErrors(e) {
    throw new Error(e);
  };

  var sort$2 = curry(function (fn, x) {
    return x.sort(fn);
  });

  var sortLookup = pipe(toPairs, sort$2(function (_ref, _ref2) {
    var _ref3 = _slicedToArray(_ref, 2),
        v = _ref3[1];
    var _ref4 = _slicedToArray(_ref2, 2),
        w = _ref4[1];
    return w - v;
  }));

  var encode = curry(function (system, input) {
    var lookup = sortLookup(system);
    var output = "";
    forEach(function (_ref) {
      var _ref2 = _slicedToArray(_ref, 2),
          key = _ref2[0],
          value = _ref2[1];
      while (input >= value) {
        output += key;
        input -= value;
      }
    }, lookup);
    return output;
  });

  var safeNumeralSystem = curry(function (system, input) {
    return pipe(handrail_8(isNumber$1, K$1(ERRORS.NOT_A_NUMBER)), map(encode(system)), handrail_14(handleErrors, I$1))(input);
  });

  var ROMAN_NUMERAL_LOOKUP = Object.freeze({
    I: 1,
    IV: 4,
    V: 5,
    IX: 9,
    X: 10,
    XL: 40,
    L: 50,
    XC: 90,
    C: 100,
    CD: 400,
    D: 500,
    CM: 900,
    M: 1000
  });

  var romanEncode = safeNumeralSystem(ROMAN_NUMERAL_LOOKUP);

  var reducer = function reducer(system) {
    return function (sum, _ref) {
      var _ref2 = _slicedToArray(_ref, 2),
          current = _ref2[0],
          next = _ref2[1];
      var value = system[current];
      return sum + value * (value < system[next] ? -1 : 1);
    };
  };

  var decode = curry(function (system, input) {
    return pipe(toUpper, split(""), addIndex(map)(function (x, i, list) {
      return [x, list[i + 1]];
    }), reduce(reducer(system), 0))(input);
  });

  var safeDecode = curry(function (system, input) {
    return pipe(handrail_8(isString$1, K$1(ERRORS.NOT_A_STRING)), map(decode(system)), handrail_14(handleErrors, I$1))(input);
  });

  var romanDecode = safeDecode(ROMAN_NUMERAL_LOOKUP);

  var index = {
    encode: safeNumeralSystem,
    decode: safeDecode,
    romanEncode: romanEncode,
    romanDecode: romanDecode
  };

  return index;

}));
